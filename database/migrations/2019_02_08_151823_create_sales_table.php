<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('member_id')->unsigned()->nullable();            
            $table->integer('item_total')->unsigned();         
            $table->bigInteger('price_total')->unsigned();           
            $table->integer('discount_member')->unsigned();       
            $table->bigInteger('amount')->unsigned();          
            $table->bigInteger('paid')->unsigned();          
            $table->integer('user_id')->unsigned();     
            $table->timestamps();
        });

        Schema::create('sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id')->unsigned();       
            $table->bigInteger('product_id')->unsigned();         
            $table->bigInteger('sell_price')->unsigned();         
            $table->integer('qty')->unsigned();    
            $table->integer('discount')->unsigned();                 
            $table->bigInteger('sub_total')->unsigned();     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sale_details');
    }
}
