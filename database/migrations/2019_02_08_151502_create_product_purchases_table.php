<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id')->unsigned();            
            $table->integer('item_total')->unsigned();         
            $table->bigInteger('price_total')->unsigned();           
            $table->integer('discount')->unsigned();       
            $table->bigInteger('paid')->unsigned();
            $table->timestamps();
        });

        Schema::create('product_purchase_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_purchase_id')->unsigned();         
            $table->bigInteger('product_id')->unsigned();           
            $table->bigInteger('base_price')->unsigned();           
            $table->integer('qty')->unsigned();             
            $table->bigInteger('sub_price')->unsigned();     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_purchases');
        Schema::dropIfExists('product_purchase_detail');
    }
}
