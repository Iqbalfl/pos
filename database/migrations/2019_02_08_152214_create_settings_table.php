<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('store_name', 100);       
            $table->string('store_address');       
            $table->string('store_message')->nullable();       
            $table->string('store_phone', 20)->nullable();           
            $table->string('store_logo', 50);                
            $table->string('member_card_image', 50);                
            $table->integer('member_discount')->unsigned();                
            $table->string('printer_name', 50);       
            $table->boolean('receipt')->default(1);
            $table->boolean('cashdrawer')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
