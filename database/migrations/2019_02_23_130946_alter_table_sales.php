<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->string('invoice')->after('id')->nullable();
            $table->softdeletes();
        });
        Schema::table('sale_details', function (Blueprint $table) {
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('invoice');
            $table->dropColumn('deleted_at');
        });
        Schema::table('sale_details', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
