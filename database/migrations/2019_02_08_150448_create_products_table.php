<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();           
            $table->integer('category_id')->unsigned();           
            $table->string('name', 150);           
            $table->string('brand', 50);             
            $table->bigInteger('base_price')->unsigned();         
            $table->integer('discount')->unsigned();             
            $table->bigInteger('sell_price')->unsigned();          
            $table->tinyinteger('status')->default(100);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
