<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_purchases', function (Blueprint $table) {
            $table->string('invoice')->after('id')->nullable();
            $table->renameColumn('paid', 'amount');
            $table->integer('user_id')->after('paid')->unsigned();  
            $table->softdeletes();
        });

        Schema::table('product_purchase_details', function (Blueprint $table) {
            $table->renameColumn('sub_price', 'sub_total');
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_purchases', function (Blueprint $table) {
            $table->renameColumn('amount', 'paid');
            $table->dropColumn('user_id');
            $table->dropColumn('invoice');
            $table->dropColumn('deleted_at');
        });

        Schema::table('product_purchase_details', function (Blueprint $table) {
            $table->renameColumn('sub_total', 'sub_price');
            $table->dropColumn('deleted_at');
        });
    }
}
