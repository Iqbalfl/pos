/*
 Navicat Premium Data Transfer

 Source Server         : laragon-mysql
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 127.0.0.1:3306
 Source Schema         : pos

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 24/02/2019 10:23:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Makanan Ringan', '2019-02-12 22:41:26', '2019-02-12 22:41:28');
INSERT INTO `categories` VALUES (3, 'Minuman', '2019-02-12 16:21:08', '2019-02-12 16:39:19');
INSERT INTO `categories` VALUES (5, 'Obat-obatan', '2019-02-12 16:40:49', '2019-02-13 14:49:37');

-- ----------------------------
-- Table structure for expenditures
-- ----------------------------
DROP TABLE IF EXISTS `expenditures`;
CREATE TABLE `expenditures`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of expenditures
-- ----------------------------
INSERT INTO `expenditures` VALUES (1, 'Bayar Listrik', 500000, '2019-02-15', '2019-02-15 22:31:02', '2019-02-15 22:31:02');

-- ----------------------------
-- Table structure for inventories
-- ----------------------------
DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventories
-- ----------------------------
INSERT INTO `inventories` VALUES (1, 7, 102, 100, '2019-02-14 19:36:22', '2019-02-23 22:24:59');
INSERT INTO `inventories` VALUES (2, 1, 78, 100, '2019-02-14 19:43:19', '2019-02-23 22:25:48');

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES (1, 178619890790, 'Muhamad Iqbal', 'Cibiru - Bandung', '089765432987', '2019-02-15 21:09:08', '2019-02-15 21:09:10');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_02_08_150224_create_categories_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_02_08_150448_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_02_08_150620_create_suppliers_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_02_08_150657_create_members_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_02_08_151502_create_product_purchases_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_02_08_151823_create_sales_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_02_08_152145_create_expenditures_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_02_08_152214_create_settings_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_02_08_152622_create_inventories_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_02_23_130946_alter_table_sales', 2);
INSERT INTO `migrations` VALUES (16, '2019_02_23_170828_alter_table_product_purchases', 3);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_purchase_details
-- ----------------------------
DROP TABLE IF EXISTS `product_purchase_details`;
CREATE TABLE `product_purchase_details`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_purchase_id` int(10) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `base_price` bigint(20) UNSIGNED NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL,
  `sub_total` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_purchase_details
-- ----------------------------
INSERT INTO `product_purchase_details` VALUES (1, 3, 7, 5000, 10, 50000, '2019-02-23 20:28:36', '2019-02-23 22:24:59', NULL);
INSERT INTO `product_purchase_details` VALUES (2, 3, 1, 1000, 10, 10000, '2019-02-23 20:28:36', '2019-02-23 22:25:48', '2019-02-23 22:25:48');
INSERT INTO `product_purchase_details` VALUES (3, 4, 7, 5000, 10, 50000, '2019-02-23 21:45:51', '2019-02-23 22:06:03', '2019-02-23 22:06:03');

-- ----------------------------
-- Table structure for product_purchases
-- ----------------------------
DROP TABLE IF EXISTS `product_purchases`;
CREATE TABLE `product_purchases`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `price_total` bigint(20) UNSIGNED NOT NULL,
  `discount` int(10) UNSIGNED NOT NULL,
  `amount` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_purchases
-- ----------------------------
INSERT INTO `product_purchases` VALUES (3, 'PRC-1902-5FSEBV', 1, 10, 50000, 5, 47500, 1, '2019-02-23 20:28:36', '2019-02-23 22:25:48', NULL);
INSERT INTO `product_purchases` VALUES (4, 'PRC-1902-1Q0LBX', 1, 10, 50000, 0, 50000, 1, '2019-02-23 21:45:51', '2019-02-23 22:06:03', '2019-02-23 22:06:03');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_price` bigint(20) UNSIGNED NOT NULL,
  `discount` int(10) UNSIGNED NOT NULL,
  `sell_price` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `products_code_unique`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 15676896879, 1, 'Lays', 'Lays', 4000, 2, 6000, 100, '2019-02-13 22:48:08', '2019-02-22 18:49:10');
INSERT INTO `products` VALUES (7, 15676896878, 3, 'Proman', 'Proman', 5000, 0, 7000, 100, '2019-02-14 19:36:22', '2019-02-24 10:17:33');

-- ----------------------------
-- Table structure for sale_details
-- ----------------------------
DROP TABLE IF EXISTS `sale_details`;
CREATE TABLE `sale_details`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sale_id` int(10) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `sell_price` bigint(20) UNSIGNED NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL,
  `discount` int(10) UNSIGNED NOT NULL,
  `sub_total` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sale_details
-- ----------------------------
INSERT INTO `sale_details` VALUES (2, 4, 7, 7000, 2, 0, 14000, '2019-02-19 22:57:36', '2019-02-23 14:46:57', NULL);
INSERT INTO `sale_details` VALUES (3, 4, 1, 1000, 2, 0, 2000, '2019-02-19 22:57:36', '2019-02-23 21:42:32', NULL);
INSERT INTO `sale_details` VALUES (4, 5, 7, 7000, 3, 0, 21000, '2019-02-19 23:03:40', '2019-02-19 23:03:40', NULL);
INSERT INTO `sale_details` VALUES (5, 5, 1, 1000, 2, 0, 2000, '2019-02-19 23:03:40', '2019-02-19 23:03:40', NULL);
INSERT INTO `sale_details` VALUES (6, 6, 7, 7000, 2, 0, 14000, '2019-02-20 21:57:57', '2019-02-20 21:57:57', NULL);
INSERT INTO `sale_details` VALUES (7, 7, 7, 7000, 2, 0, 14000, '2019-02-20 21:58:24', '2019-02-23 13:19:51', '2019-02-23 13:19:51');
INSERT INTO `sale_details` VALUES (8, 8, 7, 7000, 1, 0, 7000, '2019-02-23 20:46:05', '2019-02-23 20:46:05', NULL);
INSERT INTO `sale_details` VALUES (9, 8, 1, 6000, 2, 2, 11760, '2019-02-23 20:46:05', '2019-02-23 20:46:05', NULL);

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `member_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `price_total` bigint(20) UNSIGNED NOT NULL,
  `discount_member` int(10) UNSIGNED NOT NULL,
  `amount` bigint(20) NOT NULL,
  `paid` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES (4, 'INV-1902-KPAXGV', 1, 4, 16000, 5, 15200, 20000, 1, '2019-02-19 22:57:36', '2019-02-23 21:42:33', NULL);
INSERT INTO `sales` VALUES (5, 'INV-1902-PNS26B', NULL, 5, 23000, 0, 23000, 23000, 2, '2019-02-19 23:03:40', '2019-02-19 23:03:40', NULL);
INSERT INTO `sales` VALUES (6, 'INV-1902-2PTF0Z', NULL, 2, 14000, 0, 14000, 14000, 2, '2019-02-20 21:57:57', '2019-02-20 21:57:57', NULL);
INSERT INTO `sales` VALUES (7, 'INV-1902-3SF1SH', 1, 2, 14000, 5, 13300, 14000, 2, '2019-02-20 21:58:24', '2019-02-23 13:19:51', '2019-02-23 13:19:51');
INSERT INTO `sales` VALUES (8, 'INV-1902-34JLI8', NULL, 3, 18760, 0, 18760, 20000, 2, '2019-02-23 20:46:05', '2019-02-23 20:46:05', NULL);

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `store_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `store_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `store_logo` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_card_image` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_discount` int(10) UNSIGNED NOT NULL,
  `printer_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `receipt` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `cashdrawer` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (0000000001, 'Krusty Mart', 'Jl. Jakarta No. 15  Bandung', 'Terima Kasih Telah Berbelanja di Krusty Mart', '082786542872', '7d9513af1e79b4c425c0266f56cd71a6.png', '33e316b4cf87d7875054605ae76995c6.png', 5, 'POS-58', 1, 1, '2019-02-19 20:15:38', '2019-02-24 06:20:33');

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (1, 'CV. Indah Nusa', 'Dago - Bandung', '089765656572', '2019-02-15 22:07:29', '2019-02-15 22:08:47');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cashier',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Muhamad Iqbal', 'iqbal@gmail.com', NULL, '$2y$10$P7iK69efBzJ/ofsNzr10HOcrUwRq1BuLwtfWDBJlbdZ0Kh4XCAqLG', 'user-default.jpg', 'admin', 'ZRsHkDwawIgesnKwYAdtwiRmKqmUT9vld3BLapGHTQZgVORDYjecHr2KgK5P', '2019-02-12 14:50:31', '2019-02-12 14:50:31');
INSERT INTO `users` VALUES (2, 'Nabilah Ayu', 'nabilah@gmail.com', NULL, '$2y$10$P7iK69efBzJ/ofsNzr10HOcrUwRq1BuLwtfWDBJlbdZ0Kh4XCAqLG', 'user-default.jpg', 'cashier', 'sF7ne1gh3EtDyXyuwunuXNjlbT4d2qGMm3LgvF7yLH8jPEcMqalkIU3t1gEJ', '2019-02-16 20:40:26', '2019-02-16 20:46:40');
INSERT INTO `users` VALUES (4, 'Melody Nuramdani', 'melody@gmail.com', NULL, '$2y$10$D2LMqd6OLv4jksWP8v/bP.oD16goMhEYtAvRZ1bJ.3PEZiWIurDJ6', 'user-default.jpg', 'warehouse', 'ioPdXalCz9e6Wi1OgHAGFRKkDk3y51j9OTIMi8ABj9WbmeHhd3qK7eP5yBOA', '2019-02-16 20:47:55', '2019-02-16 20:49:57');

SET FOREIGN_KEY_CHECKS = 1;
