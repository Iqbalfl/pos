<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
     
			<form class="form-horizontal" data-toggle="validator" method="post">
				{{ csrf_field() }} {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearError()"><span aria-hidden="true"> &times; </span> </button>
					<h3 class="modal-title"></h3>
				</div>
									
				<div class="modal-body">
					
					<input type="hidden" id="id" name="id">
					
					<div class="form-group">
						<label for="name" class="col-md-3 control-label">Nama Supplier</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" autofocus>
							<span class="help-block with-errors"><strong id="name-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
						<label for="address" class="col-md-3 control-label">Alamat</label>
						<div class="col-md-6">
							<input id="address" type="text" class="form-control" name="address">
							<span class="help-block with-errors"><strong id="address-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
						<label for="phone" class="col-md-3 control-label">Telepon</label>
						<div class="col-md-6">
							<input id="phone" type="text" class="form-control" name="phone" autofocus>
							<span class="help-block with-errors"><strong id="phone-error"></strong></span>
						</div>
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
					<button type="button" class="btn btn-warning" data-dismiss="modal" onclick="clearError()"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>

			</form>

		</div>
	</div>
</div>
