<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
     
			<form class="form-horizontal" data-toggle="validator" method="post">
				{{ csrf_field() }} {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearError()"><span aria-hidden="true"> &times; </span> </button>
					<h3 class="modal-title"></h3>
				</div>
									
				<div class="modal-body">
					
					<input type="hidden" id="id" name="id">
					
					<div class="form-group">
						<label for="date" class="col-md-3 control-label">Tanggal</label>
						<div class="col-md-6">
							<input id="date" type="text" class="form-control" name="date" autofocus>
							<span class="help-block with-errors"><strong id="date-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
						<label for="type" class="col-md-3 control-label">Jenis Pengeluaran</label>
						<div class="col-md-6">
							<input id="type" type="text" class="form-control" name="type">
							<span class="help-block with-errors"><strong id="type-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
						<label for="nominal" class="col-md-3 control-label">Nominal</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input id="nominal" type="text" class="form-control money" name="nominal" autofocus>
							</div>
							<span class="help-block with-errors"><strong id="nominal-error"></strong></span>
						</div>
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
					<button type="button" class="btn btn-warning" data-dismiss="modal" onclick="clearError()"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>

			</form>

		</div>
	</div>
</div>
