@extends('layouts.main')

@section('title')
  Daftar Pengeluaran
@endsection

@section('breadcrumb')
   @parent
   <li>Pengeluaran</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
      </div>
      <div class="box-body">  
				<table class="table table-striped datatable">
				<thead>
					<tr>
						<th width="30">No</th>
						<th>Tanggal</th>
						<th>Jenis Pengeluaran</th>
						<th>Nominal</th>
						<th width="100">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
				</table>
      </div>
    </div>
  </div>
</div>

@include('expenditures.form')
@endsection

@section('script')
<script type="text/javascript">
	var table, save_method;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ajax: '{{ route('expenditure.index') }}',
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'date_format', name: 'date'},
				{data: 'type', name: 'type'},
				{data: 'nominal_f', name: 'nominal'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

		$('#modal-form form').on('submit', function(e){
			if(!e.isDefaultPrevented()){
				clearError();

				var id = $('#id').val();
				if(save_method == "add") 
					url = "{{ route('expenditure.store') }}";
				else 
					url = "expenditure/"+id;
				
				$.ajax({
					url : url,
					type : "POST",
					data : $('#modal-form form').serialize(),
					success : function(data){
						console.log(data);
						if(data.errors) {
							if(data.errors.date){
								$('#date-error').html(data.errors.date[0]);
							}
							if(data.errors.type){
								$('#type-error').html(data.errors.type[0]);
							}
							if(data.errors.nominal){
								$('#nominal-error').html(data.errors.nominal[0]);
							}
						}
						if(data.success) {
							$('#modal-form').modal('hide');
							table.ajax.reload();
						}
					},
					error : function(){
						alert("Tidak dapat menyimpan data!");
					}   
				});
				return false;
			}
		});

		$('#date').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.money').mask('000.000.000', {reverse: true});

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});

	function clearError() {
		$('#date-error').html("");
		$('#type-error').html("");
		$('#nominal-error').html("");
	}

	function addForm(){
		save_method = "add";
		$('input[name=_method]').val('POST');
		$('#modal-form').modal('show');
		$('#modal-form form')[0].reset();            
		$('.modal-title').text('Tambah Pengeluaran');
	}

	function editForm(id){
		save_method = "edit";
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();
		$.ajax({
			url : "expenditure/"+id+"/edit",
			type : "GET",
			dataType : "JSON",
			success : function(data){
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit Pengeluaran');
				
				$('#id').val(data.id);
				$('#date').val(data.date);
				$('#type').val(data.type);
				$('#nominal').val(data.nominal);
				
			},
			error : function(){
				alert("Tidak dapat menampilkan data!");
			}
		});
	}
</script>
@endsection