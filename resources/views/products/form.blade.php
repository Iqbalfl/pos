<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
	
			<form class="form-horizontal" data-toggle="validator" method="post">
			{{ csrf_field() }} {{ method_field('POST') }}
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearError()"><span aria-hidden="true"> &times; </span> </button>
				<h3 class="modal-title"></h3>
			</div>
						
			<div class="modal-body">
				
				<input type="hidden" id="id" name="id">
				<div class="form-group">
					<label for="code" class="col-md-3 control-label">Kode Produk</label>
					<div class="col-md-6">
						<input id="code" type="number" class="form-control" name="code" autofocus >
						<span class="help-block with-errors"><strong id="code-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="name" class="col-md-3 control-label">Nama Produk</label>
					<div class="col-md-6">
						<input id="name" type="text" class="form-control" name="name" >
						<span class="help-block with-errors"><strong id="name-error"></strong></span>
					</div>
				</div>
				<input type="hidden" name="selected-category" value="0">
				<div class="form-group">
					<label for="category" class="col-md-3 control-label">Kategori</label>
					<div class="col-md-6">
						<select id="category" type="text" class="form-control" name="category" >
							<option value=""> -- Pilih Kategori-- </option>
							@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
						<span class="help-block with-errors"><strong id="category-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="brand" class="col-md-3 control-label">Merk</label>
					<div class="col-md-6">
						<input id="brand" type="text" class="form-control" name="brand" >
						<span class="help-block with-errors"><strong id="brand-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="base_price" class="col-md-3 control-label">Harga Beli</label>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input id="base_price" type="text" class="form-control money" name="base_price" >
						</div>
						<span class="help-block with-errors"><strong id="base_price-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="discount" class="col-md-3 control-label">Diskon</label>
					<div class="col-md-2">
						<div class="input-group">
							<input id="discount" type="text" class="form-control" name="discount" >
							<span class="input-group-addon">%</span>
						</div>
						<span class="help-block with-errors"><strong id="discount-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="sell_price" class="col-md-3 control-label">Harga Jual</label>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input id="sell_price" type="text" class="form-control money" name="sell_price" >
						</div>
						<span class="help-block with-errors"><strong id="sell_price-error"></strong></span>
					</div>
				</div>

				<div class="form-group">
					<label for="stock" class="col-md-3 control-label">Stok</label>
					<div class="col-md-2">
						<input id="stock" type="text" class="form-control" name="stock" >
						<span class="help-block with-errors"><strong id="stock-error"></strong></span>
					</div>
				</div>
				
			</div>
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
				<button type="button" class="btn btn-warning" data-dismiss="modal" onclick="clearError()"><i class="fa fa-arrow-circle-left"></i> Batal</button>
			</div>
			
			</form>

		</div>
	</div>
</div>