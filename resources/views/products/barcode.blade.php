<!DOCTYPE html>
<html>
<head>
   <title>Cetak Barcode</title>
</head>
<body>
   <table width="100%">   
     <tr>
      
      @foreach($products as $data)
      <td align="center" style="border: 1px solid #ccc">
      {{ $data->name}} - Rp. {{ rupiah_format($data->sell_price) }}</span><br>
      <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG( $data->code, 'C39') }}" height="60" width="180">
      <br>{{ $data->code}}
      </td>
      @if( $no++ % 3 == 0)
         </tr><tr>
      @endif
     @endforeach
     
     </tr>
   </table>
</body>
</html>