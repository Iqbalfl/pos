@extends('layouts.main')

@section('title')
  Daftar Produk
@endsection

@section('breadcrumb')
   @parent
   <li>Produk</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
				@if (\Auth::user()->hasRole('admin') || \Auth::user()->hasRole('warehouse'))	
					<a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
				@endif
				@if (\Auth::user()->hasRole('admin'))	
					<a onclick="deleteAll()" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus Terpilih</a>
				@endif
				<a onclick="printBarcode()" class="btn btn-info"><i class="fa fa-barcode"></i> Cetak Barcode</a>
      </div>
      <div class="box-body">
				<form method="post" id="form-product">
					{{ csrf_field() }}
					<table class="table table-striped datatable">
						<thead>
						<tr>
							<th width="20"><input type="checkbox" value="1" id="select-all"></th>
							<th width="20">No</th>
							<th>Kode Produk</th>
							<th>Nama Produk</th>
							<th>Kategori</th>
							<th>Merk</th>
							<th>Harga Beli</th>
							<th>Harga Jual</th>
							<th>Diskon</th>
							<th>Stok</th>
							<th>Status</th>
							<th width="100">Aksi</th>
						</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
      </div>
    </div>
  </div>
</div>
@include('products.form')
@endsection

@section('script')
<script>
	var table, save_method;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
				processing: true,
				serverSide: true,
				autoWidth: false,
				ajax: '{{ route('product.index') }}',
				columns: [
					{data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
					{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
					{data: 'code', name: 'products.code'},
					{data: 'name', name: 'products.name'},
					{data: 'category.name', name: 'category.name'},
					{data: 'brand', name: 'products.brand'},
					{data: 'base_price_f', name: 'products.base_price'},
					{data: 'sell_price_f', name: 'products.sell_price'},
					{data: 'discount_f', name: 'products.discount'},
					{data: 'inventory.stock', name: 'inventory.stock', defaultContent: 0, searchable: false},
					{data: 'status_string', name: 'products.status', searchable: false},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
		});

		$('#select-all').click(function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
   	});

		$('.money').mask('000.000.000', {reverse: true});

		$('#modal-form form').on('submit', function(e){
				if(!e.isDefaultPrevented()){
					clearError();
					if ($('#stock').val() == '') {
							$('#stock').val(0);
					}
					if ($('#discount').val() == '') {
							$('#discount').val(0);
					}
					var id = $('#id').val();
					var message;
					if(save_method == "add") {
						url = "{{ route('product.store') }}";
						message = "menambahkan";
					} else { 
						url = "product/"+id;
						message = "mengubah";
					}
					
					$.ajax({
						url : url,
						type : "POST",
						data : $('#modal-form form').serialize(),
						success : function(data){
							if(data.errors) {
								if(data.errors.code){
									$('#code-error').html(data.errors.code[0]);
								}
								if(data.errors.name){
									$('#name-error').html(data.errors.name[0]);
								}
								if(data.errors.category){
									$('#category-error').html(data.errors.category[0]);
								}
								if(data.errors.brand){
									$('#brand-error').html(data.errors.brand[0]);
								}
								if(data.errors.base_price){
									$('#base_price-error').html(data.errors.base_price[0]);
								}
								if(data.errors.discount){
									$('#discount-error').html(data.errors.discount[0]);
								}
								if(data.errors.sell_price){
									$('#sell_price-error').html(data.errors.sell_price[0]);
								}
								if(data.errors.stock){
									$('#stock-error').html(data.errors.stock[0]);
								}
              }
							if(data.success) {
								$('#modal-form').modal('hide');
								toastr.success("Berhasil "+message+" produk");
								table.ajax.reload();
							}
						},
						error : function(){
							toastr.erorr("Terjadi Kesalahan, tidak dapat menyimpan data!");
						}   
					});
					return false;
			}
		});

		$(document).on('click','.js-submit-confirm', function(e){
				e.preventDefault();
				swal({
					title: 'Apakah anda yakin ingin menghapus?',
					text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
					icon: 'warning',
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						$(this).closest('form').submit();
					} 
				});
		});
	});

	function addForm(){
			save_method = "add";
			$('input[name=_method]').val('POST');
			$('#modal-form').modal('show');
			$('#modal-form form')[0].reset();            
			$('.modal-title').text('Tambah Produk');
	};

	function clearError() {
			$('#code-error').html("");
			$('#name-error').html("");
			$('#category-error').html("");
			$('#brand-error').html("");
			$('#base_price-error').html("");
			$('#discount-error').html("");
			$('#sell_price-error').html("");
			$('#stock-error').html("");
	}

	function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PATCH');
   $('#modal-form form')[0].reset();
   $.ajax({
     url : "product/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-form').modal('show');
       $('.modal-title').text('Edit Produk');
       
       $('#id').val(data.id);
			 $('#code').val(data.code);
       $('#name').val(data.name);
       $('#category').val(data.category_id);
       $('#brand').val(data.brand);
       $('#base_price').val(data.base_price);
       $('#discount').val(data.discount);
       $('#sell_price').val(data.sell_price);
       $('#stock').val(data.inventory.stock);
     },
     error : function(){
			 toastr.erorr("Terjadi Kesalahan, tidak dapat menampilkan data!");
     }
   });
	}

	function deleteAll(){
		if($('input:checked').length < 1){
			alert('Pilih data yang akan dihapus!');
		}else if(confirm("Apakah yakin akan menghapus semua data terpilih?")){
			$.ajax({
				url : "/main/product/delete-selected",
				type : "POST",
				data : $('#form-product').serialize(),
				success : function(data){
					if(data.success) {
						toastr.success("Berhasil menghapus produk terpilih");
					}
					if(data.cannot) {
						toastr.warning("Produk : "+data.cannot+" tidak bisa dihapus karena sudah memiliki penjualan!");
					}
					table.ajax.reload();
					$('#select-all').prop('checked', false);
				},
				error : function(){
					toastr.erorr("Gagal Terjadi Kesalahan");
				}
			});
		}
	}

	function deactivate(id) {
		$.ajax({
				url : "/main/product/"+id+"/activate",
				type : "POST",
				data : {'status' : 0},
				headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      	},
				success : function(data){
					toastr.success("Berhasil menonaktifkan produk");
					table.ajax.reload();
				},
				error : function(){
					toastr.erorr("Gagal Terjadi Kesalahan");
				}
			});
	}

	function activate(id) {
			$.ajax({
				url : "/main/product/"+id+"/activate",
				type : "POST",
				data : {'status' : 100},
				headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      	},
				success : function(data){
					toastr.success("Berhasil mengaktifkan produk");
					table.ajax.reload();
				},
				error : function(){
					toastr.erorr("Gagal Terjadi Kesalahan");
				}
			});
	}

	function printBarcode(){
		if($('input:checked').length < 1){
			alert('Pilih data yang akan dicetak!');
		}else{
			$('#form-product').attr('target', '_blank').attr('action', "/main/product/print-barcode").submit();
		}
	}
</script>
@endsection