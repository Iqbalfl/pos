@extends('layouts.main')

@section('title')
  Edit Pembelian | Invoice : {{$purchase->invoice ?? '-'}}
@endsection

@section('breadcrumb')
   @parent
   <li>Edit Pembelian</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post" action="{{ route('purchase.update', $purchase->id) }}" enctype="multipart/form-data">
		{{ csrf_field() }} {{ method_field('PATCH') }}
		<div class="col-md-8">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Barang</h3>
				</div>
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered tabel-detail">
							<thead>
								<tr>
									<th width="30">No</th>
									<th>Kode Produk</th>
									<th>Nama Produk</th>
									<th align="right">Harga Beli</th>
									<th>Jumlah</th>
									<th align="right">Sub Total</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody></tbody>   
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ringkasan</h3>
				</div>
				<div class="box-body">
					<input type="hidden" id="item_total" name="item_total" value="{{$purchase->item_total}}">
					<div class="form-group">
						<label for="price_total" class="col-md-4 control-label">Total</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="price_total" name="price_total" value="{{rupiah_format($purchase->price_total)}}" readonly>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="discount" class="col-md-4 control-label">Diskon</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" class="form-control" name="discount" id="discount" value="{{$purchase->discount}}" readonly>
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="amount" class="col-md-4 control-label">Bayar</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="amount" name="amount" value="{{ rupiah_format($purchase->amount)}}" readonly>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>
		</div>
	</form>
</div>

@endsection

@section('script')
<script type="text/javascript">
	var table, table_detail;
	$(document).ready(function() {
		
		table_detail = $('.tabel-detail').DataTable({
			dom : 'Brt',
			bSort : false,
			autoWidth : false,
			processing : true,
			ajax: '{{ route('purchase.edit', $purchase->id) }}',
    });

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});

	function deleteItem(id){
		if(confirm("Apakah yakin data akan dihapus?")){
			if ($('#item_count').val() == 1) {
				return alert('Tidak dapat menghapus semua barang!');
			}
			$.ajax({
				url : "{{route('purchase.update', $purchase->id)}}",
				type : "POST",
				data : {'_method' : 'PATCH', '_token' : $('meta[name=csrf-token]').attr('content'), 'delete_id' : id},
				success : function(data){
					window.location.reload(); 
				},
				error : function(){
					alert("Tidak dapat menghapus data!");
				}
			});
		}
	}

	function changeCount(id){
		$.ajax({
			url : "{{route('purchase.update', $purchase->id)}}",
			type : "POST",
			data : {'_method' : 'PATCH', '_token' : $('meta[name=csrf-token]').attr('content'), 'update_id' : id, 'qty' : $('#qty_'+id).val()},
			success : function(data){
				window.location.reload();           
			},
			error : function(){
				alert("Tidak dapat menyimpan data!");
			}   
		});
	}
</script>
@endsection