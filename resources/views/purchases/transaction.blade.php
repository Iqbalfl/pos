@extends('layouts.main')

@section('title')
  Transaksi Pembelian
@endsection

@section('breadcrumb')
   @parent
   <li>Pembelian</li>
   <li>Transaksi</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box"> 
      <div class="box-body">
				@if ($supplier == null)
						<script>
							alert('Suplier tidak ditemukan!');
							window.location.href = '{{ route('purchase.index') }}';
						</script>
				@endif
				<table>
					<tr><td width="150">Supplier</td><td><b>{{ $supplier->name ?? ''}}</b></td></tr>
					<tr><td>Alamat</td><td><b>{{ $supplier->address ?? ''}}</b></td></tr>
					<tr><td>Telpon</td><td><b>{{ $supplier->phone ?? ''}}</b></td></tr>
				</table>
				<hr>

        <form class="form form-horizontal form-product method='post'">
          <div class="form-group">
              <label class="col-md-2 control-label">Kode Produk</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input id="code-product" type="text" class="form-control" name="code-product" autofocus required>
                  <span class="input-group-btn">
                    <button onclick="showProduct()" type="button" class="btn btn-info">...</button>
                  </span>
                </div>
              </div>
          </div>
        </form>

        <section class="carts">
          @include('purchases.cart')
        </section>

        <div class="col-md-8">
          <div id="tampil-bayar" style="background: #2C3E50; color: #fff; font-size: 80px; text-align: center; height: 100px">
            
          </div>
          <div id="tampil-terbilang" style="background: #3498DB; color: #fff; font-size: 25px; text-align: center; padding: 10px"></div>
        </div>
        <div class="col-md-4">
          <form class="form form-horizontal form-pembelian" method="post" action="{{route('purchase.store')}}">
            {{ csrf_field() }}

            <input type="hidden" id="item_total" name="item_total">
            <input type="hidden" id="supplier_id" name="supplier_id" value="{{$supplier->id ?? ''}}">
            <div class="form-group">
              <label for="price_total" class="col-md-4 control-label">Total</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control" id="price_total" name="price_total" value="" readonly>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="discount" class="col-md-4 control-label">Diskon</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="number" class="form-control" name="discount" id="discount" value="0">
                  <span class="input-group-addon">%</span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="amount" class="col-md-4 control-label">Bayar</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control" id="amount" name="amount" readonly>
                </div>
              </div>
            </div>

          </form>
        </div>

      </div>

      <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
      </div>
    </div>
  </div>
</div>
@include('purchases.product')
@endsection

@section('script')
<script type="text/javascript">
  var amount = 0;
  $(document).ready(function() {
    // load table product
		$('.table-product').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ route('purchase.show-product') }}',
			columns: [
				{data: 'code', name: 'code'},
				{data: 'name', name: 'name'},
        {data: 'base_price_rupiah', name: 'base_price'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
		
    loadForm();
	});

  function showProduct(){
    $('#modal-product').modal('show');
  }

  function loadCart() {
    $.ajax({
        url : "{{ route('purchase.create') }}",
        type : "GET"
    }).done(function (data) {
        $('.carts').html(data);
        loadForm();
    }).fail(function () {
        alert('Cart could not be loaded.');
    });
  }

  function format(value) {
    var	number_string = value.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    return rupiah;
  }

  function loadForm() {
		$('#item_total').val($('#tmp_item_total').val());
    $('#price_total').val(format($('#tmp_price_total').val()));
    var discount_tmp = ($('#discount').val() / 100) * $('#tmp_price_total').val();
    amount = $('#tmp_price_total').val()-discount_tmp;
    $('#amount').val(format(amount));
		$('#tampil-bayar').html('Rp '+format(amount));

		$.ajax({
      url : "load-format",
      type : "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {'price_total' : $('#tmp_item_total').val(), 'amount' : amount},
      success : function(data){
        $('#tampil-terbilang').html(data.amount_terbilang);
      },
      error : function(){
        alert("Gagal Terjadi Kesalahan!");
      }   
    });
  }

  $('body').addClass('sidebar-collapse');

  function addItem(code) {
    $.ajax({
      url : "product/"+code,
      type : "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {},
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();
      },
      error : function(){
        alert("Kode produk tidak ditemukan!");
      }   
    });
  }

  $('#code-product').change(function(){
      code = $('#code-product').val();
      if (code != '') {
        addItem(code);
      }
  });

  $('.form-product').on('submit', function(){
      return false;
  });

	$('#discount').change(function(){
      if($(this).val() == ""){
				$(this).val(0).select();
			}
			if ($(this).val() > 100 ) {
				alert('Diskon Maksimal 100% !');
				$(this).val(0).select();
			}
      loadForm();
  });

  function selectItem(code){
    $('#modal-product').modal('hide');
    addItem(code);
  }

  function changeCount(code){
    $('#modal-product').modal('hide');
    console.log('update-'+code);

    $.ajax({
      url : "product/"+code,
      type : "PATCH",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {
        'code' : code,
        'quantity' : $('#quantity_'+code).val()
      },
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();         
      },
      error : function(){
        alert("Tidak dapat menyimpan data!");
      }   
    });
  }

  function deleteItem(code){
    $('#modal-product').modal('hide');
    console.log('delete-'+code);
    
    $.ajax({
      url : "product/"+code,
      type : "delete",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {
        'code' : code
      },
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();        
      },
      error : function(){
        alert("Tidak dapat menyimpan data!");
      }   
    });
  }

  $('.simpan').click(function(){
    $('.form-pembelian').submit();
  });

</script>
@endsection