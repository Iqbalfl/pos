@extends('layouts.main')

@section('title')
  Detail Pembelian | Invoice : {{$purchase->invoice ?? '-'}}
@endsection

@section('breadcrumb')
   @parent
   <li>Detail Pembelian</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post">
		<div class="col-md-8">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Barang</h3>
					<div class="pull-right">
						<strong>Tanggal :</strong> {{date('d F Y', strtotime($purchase->created_at))}} | <strong>Operator :</strong> {{$purchase->user->name}}
					</div>
				</div>
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered tabel-detail">
							<thead>
								<tr>
										<th width="30">No</th>
										<th>Kode Produk</th>
										<th>Nama Produk</th>
										<th align="right">Harga Beli</th>
										<th>Jumlah</th>
										<th align="right">Sub Total</th>
								</tr>
							</thead>
							<tbody></tbody>   
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ringkasan</h3>
				</div>
				<div class="box-body">
					<table align="center">
						<tr>
							<td width="100">Supplier</td>
							<td width="50"> : </td>
							<td><b>{{ $purchase->supplier->name ?? ''}}</b></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td> : </td>
							<td><b>{{ $purchase->supplier->address ?? ''}}</b></td>
						</tr>
						<tr>
							<td>Telepon</td>
							<td> : </td>
							<td><b>{{ $purchase->supplier->phone ?? ''}}</b></td>
						</tr>
					</table>
					<hr>

					<input type="hidden" id="item_total" name="item_total" value="{{$purchase->item_total}}">
					<div class="form-group">
						<label for="price_total" class="col-md-4 control-label">Total</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="price_total" name="price_total" value="{{rupiah_format($purchase->price_total)}}" readonly>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="discount" class="col-md-4 control-label">Diskon</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" class="form-control" name="discount" id="discount" value="{{$purchase->discount}}" readonly>
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="amount" class="col-md-4 control-label">Bayar</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="amount" name="amount" value="{{ rupiah_format($purchase->amount)}}" readonly>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@section('script')
<script type="text/javascript">
	var table, table_detail;
	$(document).ready(function() {
		
		table_detail = $('.tabel-detail').DataTable({
			dom : 'Brt',
			bSort : false,
			autoWidth : false,
			processing : true,
			ajax: '{{ route('purchase.show', $purchase->id) }}',
    });

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});
</script>
@endsection