<div class="table-responsive">
	<table class="table table-bordered table-striped table-sale">
		<thead>
			<tr>
				<th width="20">No</th>
				<th>Kode Produk</th>
				<th>Nama Produk</th>
				<th align="right">Harga Beli</th>
				<th width="100">Jumlah</th>
				<th align="right">Sub Total</th>
				<th>Hapus</th>
			</tr>
		</thead>
		<tbody>
				@php 
					$no=0;
					$price_total=0;
					$item_total=0;
				@endphp
			@isset($data)
				@foreach($data as $id => $detail)
					@php
						$subtotal = $detail['base_price']*$detail['quantity'];
						$price_total += $subtotal;
						$item_total += $detail['quantity'];
					@endphp
					<tr>
						<td>{{++$no}}</td>
						<td>{{$detail['code']}}</td>
						<td>{{$detail['name']}}</td>
						<td>Rp {{rupiah_format($detail['base_price'])}}</td>
						<td><input type="number" class="form-control" id="quantity_{{$detail['code']}}" onChange='changeCount("{{$detail['code']}}")' value="{{$detail['quantity']}}"></td>
						<td>Rp {{rupiah_format($subtotal)}}</td>
						<td><a onclick="deleteItem({{$detail['code']}})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>
					</tr>
				@endforeach
			@endisset
		</tbody>
	</table>
</div>

{{-- temp data --}}
<input type="hidden" id="tmp_price_total" value="{{$price_total}}">
<input type="hidden" id="tmp_item_total" value="{{$item_total}}">