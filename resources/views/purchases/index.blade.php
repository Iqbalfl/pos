@extends('layouts.main')

@section('title')
  Daftar Pembelian
@endsection

@section('breadcrumb')
   @parent
   <li>Pembelian</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
			<div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Transaksi Baru</a>
      </div>
      <div class="box-body">  

				<table class="table table-striped datatable">
					<thead>
						<tr>
							<th width="30">No</th>
							<th>Invoice</th>
							<th>Supplier</th>
							<th>Tanggal - Waktu</th>
							<th>Total Item</th>
							<th>Total Harga</th>
							<th>Diskon</th>
							<th>Total Bayar</th>
							<th>Operator</th>
							<th width="100">Aksi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>

      </div>
    </div>
  </div>
</div>
@include('purchases.supplier')
@endsection

@section('script')
<script type="text/javascript">
	var table, $table_supplier;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ajax: '{{ route('purchase.index') }}',
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'invoice', name: 'invoice'},
				{data: 'supplier.name', name: 'supplier.name'},
				{data: 'date_f', name: 'created_at'},
				{data: 'item_total', name: 'item_total'},
				{data: 'price_total_f', name: 'price_total'},
				{data: 'discount_f', name: 'discount'},
				{data: 'amount_f', name: 'amount'},
				{data: 'user.name', name: 'user.name'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
		
		table_supplier = $('.tabel-supplier').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ route('purchase.show-supplier') }}',
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'name', name: 'name'},
				{data: 'address', name: 'address'},
				{data: 'phone', name: 'phone'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});


		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});

		$('body').addClass('sidebar-collapse');
	});

	function addForm(){
		$('#modal-supplier').modal('show');        
	}

	function showDetail(id){
    window.location.href = 'purchase/'+id;
	}

	function editForm(id){
    window.location.href = 'purchase/'+id+'/edit';
	}
</script>
@endsection