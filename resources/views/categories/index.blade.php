@extends('layouts.main')

@section('title')
  Daftar Kategori
@endsection

@section('breadcrumb')
   @parent
   <li>Kategori</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
      </div>
      <div class="box-body">  
        <table class="table table-striped datatable">
					<thead>
					<tr>
							<th width="30">No</th>
							<th>Nama Kategori</th>
							<th width="100">Aksi</th>
					</tr>
					</thead>
					<tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('categories.form')
@endsection

@section('script')
<script>
	var table, save_method;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
				processing: true,
				serverSide: true,
				autoWidth: false,
				ajax: '{{ route('category.index') }}',
				columns: [
					{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
					{data: 'name', name: 'name'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
		});

		$('#modal-form form').on('submit', function(e){
				if(!e.isDefaultPrevented()){
					clearError();

					var id = $('#id').val();
					var message;
					if(save_method == "add") {
						url = "{{ route('category.store') }}";
						message = "menambahkan";
					}else {
						url = "category/"+id;
						message = "mengubah";
					}
					$.ajax({
						url : url,
						type : "POST",
						data : $('#modal-form form').serialize(),
						success : function(data){
							console.log(data);
							if(data.errors) {
								if(data.errors.name){
									$('#name-error').html(data.errors.name[0]);
								}
              }
							if(data.success) {
								$('#modal-form').modal('hide');
								toastr.success("Berhasil "+message+" kategori");
								table.ajax.reload();
							}
						},
						error : function(){
							toastr.erorr("Terjadi Kesalahan, tidak dapat menyimpan data!");
						}   
					});
					return false;
			}
		});

		$(document).on('click','.js-submit-confirm', function(e){
				e.preventDefault();
				swal({
					title: 'Apakah anda yakin ingin menghapus?',
					text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
					icon: 'warning',
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						$(this).closest('form').submit();
					} 
				});
		});
	});

	function addForm(){
			save_method = "add";
			$('input[name=_method]').val('POST');
			$('#modal-form').modal('show');
			$('#modal-form form')[0].reset();            
			$('.modal-title').text('Tambah Kategori');
	};

	function clearError() {
			$('#name-error').html("");
	}

	function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PATCH');
   $('#modal-form form')[0].reset();
   $.ajax({
     url : "category/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-form').modal('show');
       $('.modal-title').text('Edit Kategori');
       
       $('#id').val(data.id);
       $('#name').val(data.name);
     },
     error : function(){
			 toastr.erorr("Terjadi Kesalahan, Tidak dapat menampilkan data!");
     }
   });
	}
</script>
@endsection