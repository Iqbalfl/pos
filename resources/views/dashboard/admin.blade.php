@extends('layouts.main')

@section('title')
  Dashboard
@endsection

@section('breadcrumb')
   @parent
   <li>Dashboard</li>
@endsection

@section('content') 
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
            	<h3>{{ $count['product'] }}</h3>
           		<p>Total Produk</p>
            </div>
       		<div class="icon">
            	<i class="fa fa-cubes"></i>
        	</div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
            	<h3>{{ $count['supplier'] }}</h3>
           		<p>Total Supplier</p>
            </div>
       		<div class="icon">
            	<i class="fa fa-truck"></i>
        	</div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
            	<h3>{{ $count['sales'] }}</h3>
           		<p>Total Penjualan</p>
            </div>
       		<div class="icon">
            	<i class="fa fa-upload""></i>
        	</div>
        </div>
		</div>
		<div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
            	<h3>{{ $count['purchase'] }}</h3>
           		<p>Total Pembelian</p>
            </div>
       		<div class="icon">
            	<i class="fa fa-download"></i>
        	</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
            	<h3 class="box-title">Grafik Pendapatan {{ date('d-m-Y', strtotime($from)) }} s/d {{ date('d-m-Y', strtotime($to)) }}</h3>
            </div>
            <div class="box-body">
            	<div class="chart">
                    <canvas id="salesChart" style="height: 250px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Daftar 10 Barang Terlaris</h3>
			</div>
			<div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sale">
            <thead>
              <tr>
                <th width="30">#</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Kategori</th>
                <th>Terjual</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($products as $key => $item)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$item->code}}</td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->category}}</td>
                  <td>{{$item->sales}}</td>
                </tr>
              @empty
                <tr>
                  <td colspan="7"><i>Tidak Ada Data</i></td>
                </tr>
              @endforelse
            </tbody>
          </table>
        </div>
			</div>
			<!-- /.box-body -->
		</div>
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(function () {
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: {{ json_encode($date_data) }},
    datasets: [
      {
        label: "Electronics",
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgb(210, 214, 222)",
        pointColor: "rgb(210, 214, 222)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: {{ json_encode($income_data) }}
      }
    ]
  };

  var salesChartOptions = {
    pointDot: false,
    responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);
});
</script>
@endsection