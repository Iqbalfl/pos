@extends('layouts.main')

@section('title')
  Baranda
@endsection

@section('breadcrumb')
   @parent  
   <li>Dashboard</li>
@endsection

@section('content') 
<div class="row">
  <div class="col-md-12">
    <div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Message</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				Selamat datang <b><i>{{ Auth::user()->name }}</i></b><br>
				Anda Login Sebagai <strong>Operator {{ Auth::user()->role_human }}</strong>
			</div>
			<!-- /.box-body -->
		</div>
  </div>
</div>
@endsection