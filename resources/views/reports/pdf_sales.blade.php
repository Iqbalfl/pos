<!DOCTYPE html>
<html>

<head>
    <title>Laporan Laba Penjualan</title>
    <style>
        hr.new {
            border: 1px solid #ccc;
        }

        .table-striped {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .table-striped td,
        .table-striped th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        .table-striped tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .table-striped tr:hover {
            background-color: #ddd;
        }

        .table-striped th {
            padding-top: 8px;
            padding-bottom: 8px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>

<body>

    <div class="text-center">
        <strong>{{ $setting->store_name }}</strong> <br>
        <i>Alamat : {{ $setting->store_address }} | Telepon : {{ $setting->store_phone ?? '-' }}</i>

        <hr class="new">
        <strong>Laporan Laba Penjualan </strong> <br>
        <strong>Periode : </strong> {{ date('d-m-Y', strtotime($start_at)) }} s/d
        {{ date('d-m-Y', strtotime($end_at)) }}
    </div>
    <br>

    <table class="table table-bordered table-striped table-sale">
        <thead>
            <tr>
                <th width="30">#</th>
                <th>Tanggal</th>
                <th>Invoice</th>
                <th>Diskon Member</th>
                <th>Total</th>
                <th>Laba</th>
                <th>Kasir</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total_penjualan = 0;
                $total_laba = 0;
            @endphp
            @forelse ($data as $key => $item)
                @php
                    $total_penjualan += $item['total'];
                    $total_laba += $item['laba'];
                @endphp
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $item['date'] }}</td>
                    <td>{{ $item['invoice'] }}</td>
                    <td>Rp {{ rupiah_format($item['discount_member']) }}</td>
                    <td>Rp {{ rupiah_format($item['total']) }}</td>
                    <td>Rp {{ rupiah_format($item['laba']) }}</td>
                    <td>{{ $item['user'] }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7"><i>Tidak Ada Data</i></td>
                </tr>
            @endforelse
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">Total Penjualan</th>
                <th colspan="2">Rp {{ rupiah_format($total_penjualan) ?? 0 }}</th>
            </tr>
            <tr>
                <th colspan="5">Total Laba</th>
                <th colspan="2">Rp {{ rupiah_format($total_laba) ?? 0 }}</th>
            </tr>
        </tfoot>
    </table>

</body>

</html>
