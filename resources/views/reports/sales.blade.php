@extends('layouts.main')

@section('title')
  Laporan Laba Penjualan
@endsection

@section('breadcrumb')
   @parent  
   <li>Laporan</li>
   <li>Laba Penjualan</li>
@endsection

@section('content') 
<div class="row">
  <div class="col-md-12">
    <div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Laba Penjualan</h3>
				<div class="pull-right">
						<strong>Periode : </strong> {{$start_at}} s/d  {{$end_at}}
				</div>
			</div>
			<div class="box-body">
				<form class="form-inline">
					<div class="form-group">
						<label for="start_at">Tanggal Awal</label>
						<input type="text" class="form-control" id="start_at" name="start_at" value="{{$start_at}}" required>
					</div>
					<div class="form-group">
						<label for="end_at">Tanggal Akhir</label>
						<input type="text" class="form-control" id="end_at" name="end_at" value="{{$end_at}}" required>
					</div>
					<button type="submit" class="btn btn-success">Submit</button>
					<a href="{{ url('main/report/sales/pdf?start_at='.$start_at.'&end_at='.$end_at) }}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
				</form>
				<br>
				<table class="table table-bordered table-striped table-sale">
          <thead>
            <tr>
              <th width="30">#</th>
              <th>Tanggal</th>
              <th>Invoice</th>
              <th>Diskon Member</th>
              <th>Total</th>
              <th>Laba</th>
              <th>Kasir</th>
            </tr>
          </thead>
          <tbody>
            @php
                $total_penjualan = 0;
                $total_laba = 0;
            @endphp
            @forelse ($data as $key => $item)
              @php
                $total_penjualan += $item['total'];
                $total_laba += $item['laba'];
              @endphp
              <tr>
                <td>{{++$key}}</td>
                <td>{{$item['date']}}</td>
                <td>{{$item['invoice']}}</td>
                <td>Rp {{rupiah_format($item['discount_member'])}}</td>
                <td>Rp {{rupiah_format($item['total'])}}</td>
                <td>Rp {{rupiah_format($item['laba'])}}</td>
                <td>{{$item['user']}}</td>
              </tr>
            @empty
              <tr>
                <td colspan="7"><i>Tidak Ada Data</i></td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5">Total Penjualan</th>
              <th colspan="2">Rp {{rupiah_format($total_penjualan) ?? 0}}</th>	
            </tr>
            <tr>
              <th colspan="5">Total Laba <small><i>(*laba didapat dari selisih harga jual dan harga beli - diskon)</i></small></th>
              <th colspan="2">Rp {{rupiah_format($total_laba) ?? 0}}</th>	
            </tr>
          </tfoot>
        </table>
			</div>
			<!-- /.box-body -->
		</div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	var table, start_at, end_at;
	$(function(){
		$('#start_at, #end_at').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		});

	});

</script>
@endsection