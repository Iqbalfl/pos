@extends('layouts.main')

@section('title')
  Laporan Uang Masuk & Keluar
@endsection

@section('breadcrumb')
   @parent  
   <li>Laporan</li>
   <li>Uang Masuk & Keluar</li>
@endsection

@section('content') 
<div class="row">
  <div class="col-md-12">
    <div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Uang Masuk & Keluar</h3>
				<div class="pull-right">
						<strong>Periode : </strong> {{date('d-m-Y', strtotime($start_at))}} s/d  {{date('d-m-Y', strtotime($end_at))}}
				</div>
			</div>
			<div class="box-body">
				<form class="form-inline">
					<div class="form-group">
						<label for="start_at">Tanggal Awal</label>
						<input type="text" class="form-control" id="start_at" name="start_at" value="{{$start_at}}" required>
					</div>
					<div class="form-group">
						<label for="end_at">Tanggal Akhir</label>
						<input type="text" class="form-control" id="end_at" name="end_at" value="{{$end_at}}" required>
					</div>
					<button type="submit" class="btn btn-success">Submit</button>
					<a href="{{ url('main/report/income/pdf?start_at='.$start_at.'&end_at='.$end_at) }}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
				</form>
				<br>
				<table class="table table-bordered table-striped table-sale">
				<thead>
					<tr>
						<th width="30">#</th>
						<th>Tanggal</th>
						<th>Penjualan</th>
						<th>Pembelian</th>
						<th>Pengeluaran</th>
						<th>Uang Masuk</th>
						<th>Uang Keluar</th>
					</tr>
				</thead>
				<tbody>
					@php
							$total_laba = 0 ;
							$total_rugi = 0;
					@endphp
					@foreach ($data as $value)
						<tr>
							<td>{{ $value['no'] }}</td>
							<td>{{ $value['date'] }}</td>
							<td>{{ rupiah_format($value['total_sale']) }}</td>
							<td>{{ rupiah_format($value['total_purchase']) }}</td>
							<td>{{ rupiah_format($value['total_expenditure']) }}</td>
							<td>{{ rupiah_format($value['total_income']) }}</td>
							<td>{{ rupiah_format($value['total_loss']) }}</td>
						</tr>
						@php
								$total_laba += $value['total_income'];
								$total_rugi += $value['total_loss'];
						@endphp
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th colspan="5">Total Uang Masuk</th>
						<th colspan="2">Rp {{ rupiah_format($total_laba) }}</th>	
					</tr>
					<tr>
						<th colspan="5">Total Uang Keluar</th>
						<th colspan="2">Rp {{ rupiah_format($total_rugi) }}</th>	
					</tr>
				</tfoot>
			</table>
			</div>
			<!-- /.box-body -->
		</div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	var table, start_at, end_at;
	$(function(){
		$('#start_at, #end_at').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		});

	});

</script>
@endsection