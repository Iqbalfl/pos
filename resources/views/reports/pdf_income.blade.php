<!DOCTYPE html>
<html>

<head>
    <title>LAPORAN Uang Masuk & Keluar</title>
    <style>
        hr.new {
            border: 1px solid #ccc;
        }

        .table-striped {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .table-striped td,
        .table-striped th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        .table-striped tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .table-striped tr:hover {
            background-color: #ddd;
        }

        .table-striped th {
            padding-top: 8px;
            padding-bottom: 8px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>

<body>

    <div class="text-center">
        <strong>{{ $setting->store_name }}</strong> <br>
        <i>Alamat : {{ $setting->store_address }} | Telepon : {{ $setting->store_phone ?? '-' }}</i>

        <hr class="new">
        <strong>Laporan Uang Masuk & Keluar </strong> <br>
        <strong>Periode : </strong> {{ date('d-m-Y', strtotime($start_at)) }} s/d
        {{ date('d-m-Y', strtotime($end_at)) }}
    </div>
    <br>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th width="30">#</th>
                <th>Tanggal</th>
                <th>Penjualan</th>
                <th>Pembelian</th>
                <th>Pengeluaran</th>
                <th>Uang Masuk</th>
                <th>Uang Keluar</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total_laba = 0;
                $total_rugi = 0;
            @endphp
            @foreach ($data as $value)
                <tr>
                    <td>{{ $value['no'] }}</td>
                    <td>{{ $value['date'] }}</td>
                    <td>{{ rupiah_format($value['total_sale']) }}</td>
                    <td>{{ rupiah_format($value['total_purchase']) }}</td>
                    <td>{{ rupiah_format($value['total_expenditure']) }}</td>
                    <td>{{ rupiah_format($value['total_income']) }}</td>
                    <td>{{ rupiah_format($value['total_loss']) }}</td>
                </tr>
                @php
                    $total_laba += $value['total_income'];
                    $total_rugi += $value['total_loss'];
                @endphp
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">Total Uang Masuk</th>
                <th colspan="2">Rp {{ rupiah_format($total_laba) }}</th>
            </tr>
            <tr>
                <th colspan="5">Total Uang Keluar</th>
                <th colspan="2">Rp {{ rupiah_format($total_rugi) }}</th>
            </tr>
        </tfoot>
    </table>

</body>

</html>
