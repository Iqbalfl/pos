@extends('layouts.main')

@section('title')
  Pengaturan
@endsection

@section('breadcrumb')
   @parent
   <li>Pengaturan</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post" action="{{ route('setting.save') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="col-md-6">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Toko</h3>
				</div>
				<div class="box-body">
					
					<div class="form-group{{ $errors->has('store_name') ? ' has-error' : '' }}">
						<label for="store_name" class="col-md-4 control-label">Nama Toko</label>
						<div class="col-md-7">
							<input id="store_name" type="text" class="form-control" name="store_name" value="{{$data->store_name}}">
							@if ($errors->has('store_name'))
								<span class="help-block">
										<strong>{{ $errors->first('store_name') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('store_address') ? ' has-error' : '' }}">
						<label for="store_address" class="col-md-4 control-label">Alamat</label>
						<div class="col-md-7">
							<textarea name="store_address" rows="3" class="form-control">{{$data->store_address}}</textarea>
							@if ($errors->has('store_address'))
								<span class="help-block">
										<strong>{{ $errors->first('store_address') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('store_phone') ? ' has-error' : '' }}">
						<label for="store_phone" class="col-md-4 control-label">Telepon</label>
						<div class="col-md-7">
							<input id="store_phone" type="text" class="form-control" name="store_phone" value="{{$data->store_phone}}">
							@if ($errors->has('store_phone'))
								<span class="help-block">
										<strong>{{ $errors->first('store_phone') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('store_logo') ? ' has-error' : '' }}">
						<label for="store_logo" class="col-md-4 control-label">
							Logo <i class="fa fa-info-circle" data-toggle="tip-logo" data-placement="bottom" title="File logo harus memiliki resolusi 178 x 67 pixels & size tidak boleh lebih dari 1MB"></i>
						</label>
						<div class="col-md-7">
							<input id="store_logo" type="file" class="form-control" name="store_logo">
							@if ($errors->has('store_logo'))
								<span class="help-block">
										<strong>{{ $errors->first('store_logo') }}</strong>
								</span>
							@endif
							<br>
							<div class="tampil-logo">
								<img src="{{ asset('img/'.$data->store_logo) }}" width="200" class="img-thumbnail">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Member</h3>
				</div>
				<div class="box-body">
					<div class="form form-horizontal">
						<div class="form-group{{ $errors->has('member_card_image') ? ' has-error' : '' }}">
							<label for="member_card_image" class="col-md-4 control-label">
								Desain Kartu Member <i class="fa fa-info-circle" data-toggle="tip-member" data-placement="bottom" title="File image kartu member harus memiliki resolusi 626 x 184 pixels & size tidak boleh lebih dari 1MB"></i>
							</label>
							<div class="col-md-7">
								<input id="member_card_image" type="file" class="form-control" name="member_card_image">
								@if ($errors->has('member_card_image'))
									<span class="help-block">
											<strong>{{ $errors->first('member_card_image') }}</strong>
									</span>
								@endif
								<br>
								<div class="tampil-kartu">
									<img src="{{ asset('img/'.$data->member_card_image) }}" width="285" class="img-thumbnail">
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('member_discount') ? ' has-error' : '' }}">
							<label for="member_discount" class="col-md-4 control-label">Diskon Member</label>
							<div class="col-md-7">
								<div class="input-group">
                  <input type="number" class="form-control" name="member_discount" id="member_discount" value="{{$data->member_discount}}">
                  <span class="input-group-addon">%</span>
								</div>
								@if ($errors->has('member_discount'))
									<span class="help-block">
										<strong>{{ $errors->first('member_discount') }}</strong>
									</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Nota</h3>
				</div>
				<div class="box-body">
					<div class="form form-horizontal">
						<div class="form-group{{ $errors->has('receipt') ? ' has-error' : '' }}">
							<label for="receipt" class="col-md-4 control-label">Cetak Nota</label>
							<div class="col-md-7">
									<input type="radio" name="receipt" value="1" @if($data->receipt == 1) checked @endif> Ya
									<input type="radio" name="receipt" value="0" @if($data->receipt == 0) checked @endif> Tidak
									@if ($errors->has('receipt'))
										<span class="help-block">
												<strong>{{ $errors->first('receipt') }}</strong>
										</span>
									@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('store_message') ? ' has-error' : '' }}">
							<label for="store_message" class="col-md-4 control-label">
								Pesan Penutup Nota
							</label>
							<div class="col-md-7">
								<input id="store_message" type="text" class="form-control" name="store_message" value="{{$data->store_message}}">
								@if ($errors->has('store_message'))
									<span class="help-block">
											<strong>{{ $errors->first('store_message') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('printer_name') ? ' has-error' : '' }}">
							<label for="printer_name" class="col-md-4 control-label">
								Nama Printer <i class="fa fa-info-circle" data-toggle="tip-printer" data-placement="bottom" title="Printer support untuk ukuran kertas 58mm & nama printer merupakan nama printer yang di sharing"></i>
							</label>
							<div class="col-md-7">
								<input id="printer_name" type="text" class="form-control" name="printer_name" value="{{$data->printer_name}}">
								@if ($errors->has('printer_name'))
									<span class="help-block">
											<strong>{{ $errors->first('printer_name') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('cashdrawer') ? ' has-error' : '' }}">
							<label for="cashdrawer" class="col-md-4 control-label">Gunakan CashDrawer</label>
							<div class="col-md-7">
								<input type="radio" name="cashdrawer" value="1" @if($data->cashdrawer == 1) checked @endif> Ya
								<input type="radio" name="cashdrawer" value="0" @if($data->cashdrawer == 0) checked @endif> Tidak
								@if ($errors->has('cashdrawer'))
									<span class="help-block">
											<strong>{{ $errors->first('cashdrawer') }}</strong>
									</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>
		</div>
	</form>
</div>

@endsection

@section('script')
	<script>
		$(document).ready(function(){
			$('[data-toggle="tip-logo"]').tooltip(); 
			$('[data-toggle="tip-member"]').tooltip(); 
			$('[data-toggle="tip-printer"]').tooltip(); 
		});
	</script>
@endsection