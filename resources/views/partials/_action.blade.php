<form class="delete" action="{{ $form_url  }}" method="post" id="delete-form">
    {{csrf_field()}}
    {{method_field('delete')}}
    @if ($model instanceOf \App\Sale || $model instanceOf \App\ProductPurchase)
        <a onclick="showDetail({{$edit_id}})" class="btn btn-sm btn-info" title="Lihat Detail"><i class="fa fa-eye"></i></a>
    @endif

    @if (\Auth::user()->hasRole('admin'))
        <a onclick="editForm({{$edit_id}})" class="btn btn-sm btn-warning btn-edit" title="Edit Data"><i class="fa fa-edit"></i></a>
        <button type="submit" value="Delete" class="btn btn-sm btn-danger js-submit-confirm" title="Hapus Data">
            <i class="fa fa-trash"></i>
        </button>
    @endif
    @if ($model instanceOf \App\Product)
        @if ($model->status == 100)
            <a onclick="deactivate({{$edit_id}})" class="btn btn-sm btn-info" title="Nonatifkan"><i class="fa fa-check-square-o"></i></a>
        @else
            <a onclick="activate({{$edit_id}})" class="btn btn-sm btn-info" title="Aktifkan"><i class="fa fa-check-square-o"></i></a>
        @endif
    @endif
</form>