@extends('layouts.main')

@section('title')
  Daftar Penjualan
@endsection

@section('breadcrumb')
   @parent
   <li>Penjualan</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">  

				<table class="table table-striped datatable">
					<thead>
						<tr>
							<th width="30">No</th>
							<th>Invoice</th>
							<th>Tanggal - Waktu</th>
							<th>Member</th>
							<th>Total Item</th>
							<th>Total Harga</th>
							<th>Diskon Member</th>
							<th>Total Bayar</th>
							<th>Kasir</th>
							<th width="100">Aksi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>

      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	var table
	$(document).ready(function() {
		table = $('.datatable').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ajax: '{{ route('sale.index') }}',
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'invoice', name: 'invoice'},
				{data: 'date_f', name: 'created_at'},
				{data: 'member.name', name: 'member.name', "defaultContent":"<i>Tidak Ada</i>"},
				{data: 'item_total', name: 'item_total'},
				{data: 'price_total_f', name: 'price_total'},
				{data: 'discount_f', name: 'discount_member'},
				{data: 'amount_f', name: 'amount'},
				{data: 'user.name', name: 'user.name', "defaultContent":"<i>User Dihapus</i>"},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});

		$('body').addClass('sidebar-collapse');
	});

	function showDetail(id){
    window.location.href = 'sale/'+id;
	}

	function editForm(id){
    window.location.href = 'sale/'+id+'/edit';
	}
</script>
@endsection