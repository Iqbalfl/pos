@extends('layouts.main')

@section('title')
  Detail Penjualan | Invoice : {{$sale->invoice ?? '-'}}
@endsection

@section('breadcrumb')
   @parent
   <li>Detail Penjualan</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post">
		<div class="col-md-8">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Barang</h3>
					<div class="pull-right">
						<strong>Tanggal :</strong> {{date('d F Y', strtotime($sale->created_at))}} | <strong>Kasir :</strong> {{$sale->user->name ?? "User dihapus"}}
					</div>
				</div>
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered tabel-detail">
							<thead>
								<tr>
										<th width="30">No</th>
										<th>Kode Produk</th>
										<th>Nama Produk</th>
										<th align="right">Harga</th>
										<th>Jumlah</th>
										<th>Diskon Produk</th>
										<th align="right">Sub Total</th>
								</tr>
							</thead>
							<tbody></tbody>   
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ringkasan</h3>
				</div>
				<div class="box-body">
					<input type="hidden" id="item_total" name="item_total" value="{{$sale->item_total}}">
					<div class="form-group">
						<label for="price_total" class="col-md-4 control-label">Total</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="price_total" name="price_total" value="{{rupiah_format($sale->price_total)}}" readonly>
							</div>
						</div>
					</div>

					<div class="form-group{{ $errors->has('member_code') ? ' has-error' : '' }}">
						<label for="member_code" class="col-md-4 control-label">Kode Member</label>
						<div class="col-md-8">
							<input id="member_code" type="text" class="form-control" name="member_code" value="{{$sale->member->code ?? 0}}" readonly>
							@if ($errors->has('member_code'))
								<span class="help-block">
										<strong>{{ $errors->first('member_code') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label for="discount_member" class="col-md-4 control-label">Diskon Member</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" class="form-control" name="discount_member" id="discount_member" value="{{$sale->discount_member}}" readonly>
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="amount" class="col-md-4 control-label">Bayar</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="amount" name="amount" value="{{ rupiah_format($sale->amount)}}" readonly>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="paid" class="col-md-4 control-label">Diterima</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="number" class="form-control" value="{{rupiah_format($sale->paid)}}" name="paid" id="paid" readonly>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="kembali" class="col-md-4 control-label">Kembali</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input type="text" class="form-control" id="kembali" value="{{rupiah_format($sale->paid-$sale->amount)}}" readonly>
							</div>
            </div>
					</div>

          <div class="printable">
            <button type="button" class="btn btn-primary pull-right btn-print"><i class="fa fa-print"></i> Print </button>
          </div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@section('script')
<script type="text/javascript">
	var table, table_detail;
	$(document).ready(function() {
		
		table_detail = $('.tabel-detail').DataTable({
			dom : 'Brt',
			bSort : false,
			autoWidth : false,
			processing : true,
			ajax: '{{ route('sale.show', $sale->id) }}',
    });

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});

  $('.btn-print').on('click', function(e) {
    $.ajax({
        url : "{{ route('sale.print', $sale->id) }}",
        type : "GET"
    }).done(function (data) {
      swal({
				title: 'Print Berhasil!',
				text: '',
				icon: 'success',
			})
    }).fail(function () {
      swal({
				title: 'Print Gagal!',
				text: '',
				icon: 'error',
			})
    });
  })
</script>
@endsection