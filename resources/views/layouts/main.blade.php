<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<!-- Theme style -->
	<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/css/AdminLTE.min.css') }}">
	{{-- <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/modern-AdminLTE.min.css') }}"> --}}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/toastr/build/toastr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/datepicker/datepicker3.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>E</b>P</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{{ config('app.name', 'Laravel') }}</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if (!is_null(Auth::user()->avatar))
                  <img src="{{ asset('img/'.Auth::user()->avatar) }}" class="user-image" alt="User Image">
                @else
                  <img src="{{ asset('img/user-default.jpg') }}" class="user-image" alt="User Image">
                @endif
                <span class="hidden-xs">{{ Auth::user()->name }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  @if (!is_null(Auth::user()->avatar))
                    <img src="{{ asset('img/'.Auth::user()->avatar) }}" class="img-circle" alt="User Image">
                  @else
                    <img src="{{ asset('img/user-default.jpg') }}" class="img-circle" alt="User Image">
                  @endif
                  <p>
                    {{ Auth::user()->name }}
                    <small>{{ Auth::user()->email }}</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{ route('profile.show') }}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                        onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            @if (!is_null(Auth::user()->avatar))
              <img src="{{ asset('img/'.Auth::user()->avatar) }}" class="img-circle" alt="User Image">
            @else
              <img src="{{ asset('img/user-default.jpg') }}" class="img-circle" alt="User Image">
            @endif
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MENU NAVIGASI</li>

          <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

          @if( Auth::user()->hasRole('admin') )
          <li><a href="{{ route('category.index') }}"><i class="fa fa-tags"></i> <span>Kategori</span></a></li>
          <li><a href="{{ route('product.index') }}"><i class="fa fa-cube"></i> <span>Produk</span></a></li>
          {{-- <li><a href=""><i class="fa fa-cubes"></i> <span>Stok</span></a></li> --}}
          <li><a href="{{ route('member.index') }}"><i class="fa fa-credit-card"></i> <span>Member</span></a></li>
          <li><a href="{{ route('supplier.index') }}"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>
          <li><a href="{{ route('expenditure.index') }}"><i class="fa fa-money"></i> <span>Pengeluaran</span></a></li>
          <li><a href="{{ route('user.index') }}"><i class="fa fa-user"></i> <span>User</span></a></li>       
          <li><a href="{{ route('sale.index') }}"><i class="fa fa-upload"></i> <span>Penjualan</span></a></li>
          <li><a href="{{ route('purchase.index') }}"><i class="fa fa-download"></i> <span>Pembelian</span></a></li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-file-pdf-o"></i> <span>Laporan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('report.income.index') }}"><i class="fa fa-circle-o"></i> Uang Masuk & Keluar</a></li>
              <li><a href="{{ route('report.sales.index') }}"><i class="fa fa-circle-o"></i> Laba Penjualan</a></li>
              {{-- <li><a href=""><i class="fa fa-circle-o"></i> Pembelian</a></li> --}}
            </ul>
          </li>
          {{-- <li class="header">SITE</li> --}}
          <li><a href="{{ route('setting.form') }}"><i class="fa fa-gears"></i> <span>Setting</span></a></li>
          @elseif(Auth::user()->hasRole('cashier'))
          <li><a href="{{ route('product.index') }}"><i class="fa fa-cube"></i> <span>Produk</span></a></li>
          <li><a href="{{ route('transaction.index') }}"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span></a></li>
          <li><a href="{{ route('sale.index') }}"><i class="fa fa-upload"></i> <span>List Penjualan</span></a></li>
          @elseif(Auth::user()->hasRole('warehouse'))
          <li><a href="{{ route('product.index') }}"><i class="fa fa-cube"></i> <span>Produk</span></a></li>
          <li><a href="{{ route('purchase.index') }}"><i class="fa fa-download"></i> <span>Pembelian</span></a></li>
          @endif
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content  -->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          @yield('title')
        </h1>
        <ol class="breadcrumb">
          @section('breadcrumb')
          <li><a href="{{ route('home') }}"><i class="fa fa-home"></i>Home</a></li>
          @show
        </ol>
      </section>

      <section class="content">
          @yield('content')
      </section>
    </div>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Licensed</b> to {{\App\Setting::select('store_name')->first()->store_name ?? 'Company'}}
      </div>
      <strong>Copyright &copy; 2019 {{ config('app.name') }}.</strong> All rightsreserved.
    </footer>
    <!-- End Content -->

    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- DataTables -->
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('vendor/toastr/build/toastr.min.js') }} "></script>
  <script src="{{ asset('vendor/chartjs/Chart.min.js') }} "></script>
  <script src="{{ asset('vendor//datepicker/bootstrap-datepicker.js') }} "></script>
  <!-- SlimScroll -->
  <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('js/fastclick.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('vendor/adminlte/js/app.min.js') }}"></script>

  @yield('script')
  @include('partials._toast')
</body>
</html>
