@extends('layouts.main')

@section('title')
  Transaksi Penjualan
@endsection

@section('breadcrumb')
   @parent
   <li>Penjualan</li>
   <li>Transaksi</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box"> 
      <div class="box-body">

        <form class="form form-horizontal form-product method='post'">
          <div class="form-group">
              <label class="col-md-2 control-label">Kode Produk</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input id="code-product" type="text" class="form-control" name="code-product" autofocus required>
                  <span class="input-group-btn">
                    <button onclick="showProduct()" type="button" class="btn btn-info">...</button>
                  </span>
                </div>
              </div>
          </div>
        </form>

        <section class="carts">
          @include('transactions.cart')
        </section>

        <div class="col-md-8">
          <div id="tampil-bayar" style="background: #2C3E50; color: #fff; font-size: 80px; text-align: center; height: 225px">
            
          </div>
          <div id="tampil-terbilang" style="background: #3498DB; color: #fff; font-size: 25px; text-align: center; padding: 10px"></div>
        </div>
        <div class="col-md-4">
          <form class="form form-horizontal form-penjualan" method="post" action="{{route('transaction.store')}}">
            {{ csrf_field() }}

            <input type="hidden" id="item_total" name="item_total">
            <div class="form-group">
              <label for="price_total" class="col-md-4 control-label">Total</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control" id="price_total" name="price_total" value="" readonly>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="member_code" class="col-md-4 control-label">Kode Member</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input id="member-code" type="text" class="form-control" name="member_code" value="0">
                  <span class="input-group-btn">
                    <button onclick="showMember()" type="button" class="btn btn-info">Cari</button>
                  </span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="discount_member" class="col-md-4 control-label">Diskon Member</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="text" class="form-control" name="discount_member" id="discount_member" value="0" readonly>
                  <span class="input-group-addon">%</span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="amount" class="col-md-4 control-label">Bayar</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control" id="amount" name="amount" readonly>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="paid" class="col-md-4 control-label">Diterima</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control uang" value="0" name="paid" id="paid">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="kembali" class="col-md-4 control-label">Kembali</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon">Rp</span>
                  <input type="text" class="form-control" id="kembali" value="0" readonly>
                </div>
                </div>
            </div>

          </form>
        </div>

      </div>

      <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
      </div>
    </div>
  </div>
</div>
@include('transactions.product')
@include('transactions.member')
@endsection

@section('script')
<script type="text/javascript">
  var amount = 0, kembali = 0, paid = 0;
  $(document).ready(function() {
    // load table product
		$('.table-product').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ route('transaction.show-product') }}',
			columns: [
				{data: 'code', name: 'code'},
				{data: 'name', name: 'name'},
        {data: 'sell_price_rupiah', name: 'sell_price'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

    // load table member
    $('.table-member').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{{ route('transaction.show-member') }}',
			columns: [
				{data: 'code', name: 'code'},
				{data: 'name', name: 'name'},
				{data: 'address', name: 'address'},
				{data: 'phone', name: 'phone'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

    loadForm();
  });
  
  $('.uang').mask('000.000.000', {reverse: true});

  function showProduct(){
    $('#modal-product').modal('show');
  }

  function showMember(){
    $('#modal-member').modal('show');
  }

  function loadCart() {
    $.ajax({
        url : "{{ route('transaction.index') }}",
        type : "GET"
    }).done(function (data) {
        $('.carts').html(data);
        loadForm();
    }).fail(function () {
        alert('Cart could not be loaded.');
    });
  }

  function format(value) {
    var	number_string = value.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    return rupiah;
  }

  function loadForm() {
    $('#item_total').val($('#tmp_item_total').val());
    $('#price_total').val(format($('#tmp_price_total').val()));
    var discount_tmp = ($('#discount_member').val() / 100) * $('#tmp_price_total').val();
    amount = $('#tmp_price_total').val()-discount_tmp;
    $('#amount').val(format(amount));

    $.ajax({
      url : "transaction/load-format",
      type : "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {'to_rupiah' : amount, 'to_terbilang' : amount},
      success : function(data){
        $('#tampil-bayar').html('<small>Total Bayar :</small> <br>Rp '+data.rupiah_format);
        $('#tampil-terbilang').html(data.terbilang);
      },
      error : function(){
        alert("Gagal Terjadi Kesalahan!");
      }   
    });
  }

  $('body').addClass('sidebar-collapse');

  function addItem(code) {
    $.ajax({
      url : "transaction/product/"+code,
      type : "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {},
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();
      },
      error : function(){
        alert("Kode produk tidak ditemukan!");
      }   
    });
  }

  $('#code-product').change(function(){
      code = $('#code-product').val();
      if (code != '') {
        addItem(code);
      }
  });

  $('#paid').change(function() {
      paid = $(this).val().replace(/\./g, '');
      console.log(paid);
      if (paid < amount) {
        return alert('Nominal paid tidak boleh kurang dari total bayar!');
      }
      kembali = paid - amount;
      $('#kembali').val(format(kembali));

      $.ajax({
        url : "transaction/load-format",
        type : "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {'to_rupiah' : kembali, 'to_terbilang' : kembali},
        success : function(data){
          $('#tampil-bayar').html('<small>Kembali :</small> <br>Rp '+data.rupiah_format);
          $('#tampil-terbilang').html(data.terbilang);
        },
        error : function(){
          alert("Gagal Terjadi Kesalahan!");
        }   
      });      
  });

  $('.form-product').on('submit', function(){
      return false;
  });

  function selectItem(code){
    $('#modal-product').modal('hide');
    addItem(code);
  }

  function selectMember(code){
    $('#modal-member').modal('hide');
    $('#discount_member').val('{{ $setting->member_discount }}');
    $('#member-code').val(code);
    loadForm();
    $('#paid').val(0).focus().select();
  }

  $('#member-code').change(function(){
      if ($(this).val() == 0 || $(this).val() == '') {
        $('#discount_member').val(0);
        $(this).val(0);
        loadForm();
      } else {
        alert('Silahkan lakukan pencarian member dengan klik tombol Cari');
        $('#discount_member').val(0);
        $(this).val(0);
        loadForm();
      }
  });

  function changeCount(code){
    $('#modal-product').modal('hide');
    console.log('update-'+code);
    $.ajax({
      url : "transaction/product/"+code,
      type : "PATCH",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {
        'code' : code,
        'quantity' : $('#quantity_'+code).val()
      },
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();         
      },
      error : function(){
        alert("Tidak dapat menyimpan data!");
      }   
    });
  }

  function deleteItem(code){
    $('#modal-product').modal('hide');
    console.log('delete-'+code);
    $.ajax({
      url : "transaction/product/"+code,
      type : "delete",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data : {
        'code' : code
      },
      success : function(data){
        $('#code-product').val('').focus();
        loadCart();        
      },
      error : function(){
        alert("Tidak dapat menyimpan data!");
      }   
    });
  }

  $('.simpan').click(function(){
    if (paid < amount) {
      return alert('Uang diterima tidak boleh kurang dari total bayar!');
    }
    $('.form-penjualan').submit();
  });

</script>
@endsection