@extends('layouts.main')

@section('title')
  Daftar Member
@endsection

@section('breadcrumb')
   @parent
   <li>Member</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
        <a onclick="printCard()" class="btn btn-info"><i class="fa fa-credit-card"></i> Cetak Kartu</a>
      </div>
      <div class="box-body"> 
				<form method="post" id="form-member">
					{{ csrf_field() }}
					<table class="table table-striped datatable">
						<thead>
							<tr>
								<th width="20"><input type="checkbox" value="1" id="select-all"></th>
								<th width="20">No</th>
								<th>Kode Member</th>
								<th>Nama Member</th>
								<th>Alamat</th>
								<th>Telpon</th>
								<th width="100">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
      </div>
    </div>
  </div>
</div>
@include('members.form')
@endsection

@section('script')
<script type="text/javascript">
	var table, save_method;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ajax: '{{ route('member.index') }}',
			columns: [
				{data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'code', name: 'code'},
				{data: 'name', name: 'name'},
				{data: 'address', name: 'address'},
				{data: 'phone', name: 'phone'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

		$('#modal-form form').on('submit', function(e){
			if(!e.isDefaultPrevented()){
				clearError();

				var id = $('#id').val();
				if(save_method == "add") 
					url = "{{ route('member.store') }}";
				else 
					url = "member/"+id;
				
				$.ajax({
					url : url,
					type : "POST",
					data : $('#modal-form form').serialize(),
					success : function(data){
						console.log(data);
						if(data.errors) {
							if(data.errors.code){
								$('#code-error').html(data.errors.code[0]);
							}
							if(data.errors.name){
								$('#name-error').html(data.errors.name[0]);
							}
							if(data.errors.address){
								$('#address-error').html(data.errors.address[0]);
							}
							if(data.errors.phone){
								$('#phone-error').html(data.errors.phone[0]);
							}
						}
						if(data.success) {
							$('#modal-form').modal('hide');
							table.ajax.reload();
						}
					},
					error : function(){
						alert("Tidak dapat menyimpan data!");
					}   
				});
				return false;
			}
		});

		$('#select-all').click(function(){
			$('input[type="checkbox"]').prop('checked', this.checked);
		});

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});

	function clearError() {
			$('#code-error').html("");
			$('#name-error').html("");
			$('#address-error').html("");
			$('#phone-error').html("");
	}

	function addForm(){
		save_method = "add";
		$('input[name=_method]').val('POST');
		$('#modal-form').modal('show');
		$('#modal-form form')[0].reset();            
		$('.modal-title').text('Tambah Member');
		$('#kode').attr('readonly', false);
	}

	function editForm(id){
		save_method = "edit";
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();
		$.ajax({
			url : "member/"+id+"/edit",
			type : "GET",
			dataType : "JSON",
			success : function(data){
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit Member');
				
				$('#id').val(data.id);
				$('#code').val(data.code).attr('readonly', true);
				$('#name').val(data.name);
				$('#address').val(data.address);
				$('#phone').val(data.phone);
				
			},
			error : function(){
				alert("Tidak dapat menampilkan data!");
			}
		});
	}

	function printCard(){
		if($('input:checked').length < 1){
			alert('Pilih data yang akan dicetak!');
		}else{
			$('#form-member').attr('target', '_blank').attr('action', "member/print").submit();
		}
	}
</script>
@endsection