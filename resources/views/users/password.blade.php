@extends('layouts.main')

@section('title')
  Profile
@endsection

@section('breadcrumb')
   @parent
   <li>Profile</li>
   <li>Password</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post" action="{{ route('password.update') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Password</h3>
				</div>
				<div class="box-body">
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Password Lama</label>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
              <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Password Baru</label>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <input type="password" class="form-control" name="new_password" value="{{ old('new_password') }}">
                  @if ($errors->has('new_password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('new_password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="form-group">
            <label for="content" class="col-md-4 col-sm-4 col-xs-12 control-label">Konfirmasi Password Baru</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <input type="password" name="new_password_confirmation" class="form-control">
            </div>
          </div>
          <div class="form-group">
              <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                      <i class="fa fa-floppy-o"></i> Simpan
                  </button>
              </div>
          </div>
				</div>
			</div>
		
		</div>
	</form>
</div>

@endsection
