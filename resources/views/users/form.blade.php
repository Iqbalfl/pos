<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
     
			<form class="form-horizontal" data-toggle="validator" method="post">
				{{ csrf_field() }} {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearError()"><span aria-hidden="true"> &times; </span> </button>
					<h3 class="modal-title"></h3>
				</div>
									
				<div class="modal-body">
					
					<input type="hidden" id="id" name="id">
					
					<div class="form-group">
						<label for="name" class="col-md-3 control-label">Nama Lengkap</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" autofocus>
							<span class="help-block with-errors"><strong id="name-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-md-3 control-label">Email</label>
						<div class="col-md-6">
							<input id="email" type="text" class="form-control" name="email">
							<span class="help-block with-errors"><strong id="email-error"></strong></span>
						</div>
					</div>

					<div class="form-group">
							<label for="password" class="col-md-3 control-label">Password</label>
							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password">
								<span class="help-block with-errors"><strong id="password-error"></strong></span>
							</div>
					</div>

					<div class="form-group">
							<label for="password_confirmation" class="col-md-3 control-label">Konfirmasi Password</label>
							<div class="col-md-6">
								<input id="password_confirmation" type="password" class="form-control" data-match="#password" name="password_confirmation">
								<span class="help-block with-errors"></span>
							</div>
					</div>

					<div class="form-group">
						<label for="role" class="col-md-3 control-label">Hak Akses</label>
						<div class="col-md-6">
							<select name="role" id="role" class="form-control">
								<option value="admin">Administrator</option>
								<option value="warehouse">Gudang</option>
								<option value="cashier">Kasir</option>
							</select>
							<span class="help-block with-errors"><strong id="role-error"></strong></span>
						</div>
					</div>
					
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
					<button type="button" class="btn btn-warning" data-dismiss="modal" onclick="clearError()"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>

			</form>

		</div>
	</div>
</div>
