@extends('layouts.main')

@section('title')
  Daftar User
@endsection

@section('breadcrumb')
   @parent
   <li>User</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
      </div>
      <div class="box-body">  

			<table class="table table-striped datatable">
				<thead>
					<tr>
							<th width="30">No</th>
							<th>Nama User</th>
							<th>Email</th>
							<th>Avatar</th>
							<th>Hak Akses</th>
							<th width="100">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>

      </div>
    </div>
  </div>
</div>

@include('users.form')
@endsection

@section('script')
<script type="text/javascript">
	var table, save_method;
	$(document).ready(function() {
		table = $('.datatable').DataTable({
			processing: true,
			serverSide: true,
			autoWidth: false,
			ajax: '{{ route('user.index') }}',
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'},
				{data: 'avatars', name: 'avatar', orderable: false, searchable: false},
				{data: 'role_human', name: 'role'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});

		$('#modal-form form').on('submit', function(e){
			if(!e.isDefaultPrevented()){
				clearError();

				var id = $('#id').val();
				if(save_method == "add") 
					url = "{{ route('user.store') }}";
				else 
					url = "user/"+id;
				
				$.ajax({
					url : url,
					type : "POST",
					data : $('#modal-form form').serialize(),
					success : function(data){
						console.log(data);
						if(data.errors) {
							if(data.errors.name){
								$('#name-error').html(data.errors.name[0]);
							}
							if(data.errors.email){
								$('#email-error').html(data.errors.email[0]);
							}
							if(data.errors.password){
								$('#password-error').html(data.errors.password[0]);
							}
							if(data.errors.role){
								$('#role-error').html(data.errors.role[0]);
							}
						}
						if(data.success) {
							$('#modal-form').modal('hide');
							table.ajax.reload();
						}
					},
					error : function(){
						alert("Tidak dapat menyimpan data!");
					}   
				});
				return false;
			}
		});

		$(document).on('click','.js-submit-confirm', function(e){
			e.preventDefault();
			swal({
				title: 'Apakah anda yakin ingin menghapus?',
				text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$(this).closest('form').submit();
				} 
			});
		});
	});

	function clearError() {
			$('#name-error').html("");
			$('#email-error').html("");
			$('#password-error').html("");
			$('#role-error').html("");
	}

	function addForm(){
		save_method = "add";
		$('input[name=_method]').val('POST');
		$('#modal-form').modal('show');
		$('#modal-form form')[0].reset();            
		$('.modal-title').text('Tambah User');
	}

	function editForm(id){
		save_method = "edit";
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();
		$.ajax({
			url : "user/"+id+"/edit",
			type : "GET",
			dataType : "JSON",
			success : function(data){
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit User');
				
				$('#id').val(data.id);
				$('#name').val(data.name);
				$('#email').val(data.email);
				$('#role').val(data.role);
				
			},
			error : function(){
				alert("Tidak dapat menampilkan data!");
			}
		});
	}
</script>
@endsection