@extends('layouts.main')

@section('title')
  Profile
@endsection

@section('breadcrumb')
   @parent
   <li>Profile</li>
@endsection

@section('content')     
<div class="row">
	<form class="form form-horizontal" data-toggle="validator" method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Profile</h3>
				</div>
				<div class="box-body">
					
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">Nama</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" value="{{$user->name}}">
							@if ($errors->has('name'))
								<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Email</label>
						<div class="col-md-6">
							<textarea name="email" rows="3" class="form-control">{{$user->email}}</textarea>
							@if ($errors->has('email'))
								<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
						<label for="avatar" class="col-md-4 control-label">
							Avatar
						</label>
						<div class="col-md-6">
							<input id="avatar" type="file" class="form-control" name="avatar">
							<small class="pull-right"><i>*gunakan foto dengan rasio 1:1 / kotak | maximum size 1MB</i></small>
							@if ($errors->has('avatar'))
								<span class="help-block">
										<strong>{{ $errors->first('avatar') }}</strong>
								</span>
							@endif
							<br>
							<div class="tampil-logo">
								@if (!is_null($user->avatar))
									<img src="{{ asset('img/'.$user->avatar) }}" width="200" class="img-thumbnail">
								@endif
							</div>
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Password</label>
						<div class="col-md-6">
							<a href="{{route('password.show')}}" class="btn btn-default btn-sm"><i class="fa fa-lock"></i> Ubah Password</a>
							@if ($errors->has('password'))
								<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>

				</div>
			</div>
		
		</div>
	</form>
</div>

@endsection
