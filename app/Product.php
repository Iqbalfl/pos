<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $appends = ['status_string'];
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function inventory(){
        return $this->hasOne('App\Inventory', 'product_id');
    }

    public function getStatusStringAttribute()
    {
        if ($this->attributes['status'] == 100) {
            $status = 'Aktif';
        } else if ($this->attributes['status'] == 0) {
            $status = 'Non Aktif';
        } else {
            $status = $this->attributes['status'];
        }
        
        return $status;
    }
}
