<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Session;
use Illuminate\Support\Facades\File;

class SettingController extends Controller
{
    public function form(Request $request)
    {
        $setting = Setting::first();

        return view('settings.form',['data'=>$setting]);
    }
    
    public function save(Request $request){
        $this->validate($request, [
  			'store_name' => 'required|string',
  			'store_address' => 'required|string',
  			'store_message' => 'string',
  			'store_logo' => 'image|max:1024',
  			'member_card_image' => 'image|max:1024',
  			'member_discount' => 'required|numeric|max:100',
  			'receipt' => 'required|boolean',
  			'printer_name' => 'required|string',
  			'cashdrawer' => 'required|boolean',
        ]);
        
        $setting = Setting::firstOrNew(['id' => 1]);
        $setting->store_name = $request->store_name;
        $setting->store_address = $request->store_address;
        $setting->store_message = $request->store_message;
        $setting->store_phone = $request->store_phone;
        $setting->member_discount = $request->member_discount;
        $setting->receipt = $request->receipt;
        $setting->printer_name = $request->printer_name;
        $setting->cashdrawer = $request->cashdrawer;

        if ($request->hasFile('store_logo')) {
            // menambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('store_logo');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            // memindahkan file ke folder public/img
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($setting->store_logo) {
                $old_image = $setting->store_logo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                . DIRECTORY_SEPARATOR . $setting->store_logo;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $setting->store_logo = $filename;
        }

        if ($request->hasFile('member_card_image')) {
            // menambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('member_card_image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            // memindahkan file ke folder public/img
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($setting->member_card_image) {
                $old_image = $setting->member_card_image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                . DIRECTORY_SEPARATOR . $setting->member_card_image;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $setting->member_card_image = $filename;
        }

        $setting->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan setting"
        ]);

        return redirect()->route('setting.form');
    }
}
