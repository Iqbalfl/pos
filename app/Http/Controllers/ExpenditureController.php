<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Expenditure;
use Session;

class ExpenditureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $expenditures = Expenditure::query();
            return Datatables::of($expenditures)
                ->addIndexColumn()
                ->addColumn('date_format', function ($expenditure) {
                    return date('d-m-Y', strtotime($expenditure->date));
                })
                ->addColumn('nominal_f', function ($expenditure) {
                    return ('Rp '.rupiah_format($expenditure->nominal));
                })
                ->addColumn('action', function($expenditure){
                    return view('partials._action', [
                        'model'           => $expenditure,
                        'form_url'        => route('expenditure.destroy', $expenditure->id),
                        'edit_id'         => $expenditure->id
                    ]);
                })
                ->make(true);
        }

        return view('expenditures.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date' => 'required|date',
            'type' => 'required|string',
            'nominal' => 'required',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $expenditure = new Expenditure;
            $expenditure->date = $request->date;
            $expenditure->type = $request->type;
            $expenditure->nominal = str_replace(".", "", $request->nominal);
            $expenditure->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenditure = Expenditure::findOrFail($id);
        return response()->json($expenditure);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'date' => 'required|date',
            'type' => 'required|string',
            'nominal' => 'required',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $expenditure = Expenditure::find($id);
            $expenditure->date = $request->date;
            $expenditure->type = $request->type;
            $expenditure->nominal = str_replace(".", "", $request->nominal);
            $expenditure->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenditure = Expenditure::findOrFail($id);
        $expenditure->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('expenditure.index');
    }
}
