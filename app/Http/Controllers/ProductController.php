<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/* use Yajra\Datatables\Datatables; */
use Yajra\DataTables\Facades\DataTables;
use PDF;
use App\Category;
use App\Product;
use App\Inventory;
use App\SaleDetail;
use App\ProductPurchaseDetail;
use Illuminate\Support\Str;
use Session;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::with('category', 'inventory')->select('products.*');
            return Datatables::of($products)
                ->addColumn('checkbox', function ($product) {
                    return '<input type="checkbox" name="id[]" value="'.$product->id.'">';
                })
                ->addColumn('base_price_f', function ($product) {
                    return ('Rp '.rupiah_format($product->base_price));
                })
                ->addColumn('sell_price_f', function ($product) {
                    return ('Rp '.rupiah_format($product->sell_price));
                })
                ->addColumn('discount_f', function ($product) {
                    return ($product->discount.'%');
                })
                ->addIndexColumn()
                ->addColumn('action', function($product){
                    return view('partials._action', [
                        'model'           => $product,
                        'form_url'        => route('product.destroy', $product->id),
                        'edit_id'         => $product->id
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }
        $categories = Category::all();

        return view('products.index')->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'code' => 'required|numeric|unique:products',
            'name' => 'required|string',
            'category' => 'required|integer',
            'brand' => 'required|string',
            'base_price' => 'required',
            'discount' => 'required|numeric|max:100',
            'sell_price' => 'required',
            'stock' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            DB::beginTransaction();
            // Store to database  
            $product = new Product;
            $product->code = $request->code;
            $product->name = $request->name;
            $product->category_id = $request->category;
            $product->brand = $request->brand;
            $product->base_price = str_replace(".", "", $request->base_price);
            $product->discount = $request->discount;
            $product->sell_price = str_replace(".", "", $request->sell_price);
            
            if ($product->save()) {
                // create stock
                $inventory = Inventory::firstOrNew(['product_id' => $product->id]);
                $inventory->stock = $request->stock;
                $inventory->save();

                DB::commit();
            } else {
                DB::rollback();
            }

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('inventory')->findOrFail($id);
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = \Validator::make($request->all(), [
            'code' => 'required|numeric|unique:products,code,' . $id,
            'name' => 'required|string',
            'category' => 'required|integer',
            'brand' => 'required|string',
            'base_price' => 'required',
            'discount' => 'required|numeric|max:100',
            'sell_price' => 'required',
            'stock' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            DB::beginTransaction();
            // Store to database  
            $product = Product::find($id);
            $product->code = $request->code;
            $product->name = $request->name;
            $product->category_id = $request->category;
            $product->brand = $request->brand;
            $product->base_price = str_replace(".", "", $request->base_price);
            $product->discount = $request->discount;
            $product->sell_price = str_replace(".", "", $request->sell_price);
            
            if ($product->save()) {
                // create stock
                $inventory = Inventory::firstOrNew(['product_id' => $product->id]);
                $inventory->stock = $request->stock;
                $inventory->save();

                DB::commit();
            } else {
                DB::rollback();
            }

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        // cek apakah product sudah terjual
        $sale = SaleDetail::where('product_id', $id)->count();
        
        if ($sale > 0) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus produk yang sudah terjual!"
            ]);
            return redirect()->back();
        }

        $product->delete();

        // delete stock
        $inventory = Inventory::where('product_id', $product->id);
        $inventory->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('product.index');
    }

    public function deleteSelected(Request $request)
    {   
        $can = [];
        $cannot = [];

        foreach ($request->id as $id){
            $sale = SaleDetail::where('product_id', $id)->count();
            if ($sale == 0) {
                $can[] = $id;
            } else {
                $cannot[] = $id;
            }
        }

        if ($can) {
            foreach ($can as $id) {
                $product = Product::find($id);

                $inventory = Inventory::where('product_id', $id);
                $inventory->delete();

                $product->delete();
            }
            return response()->json(['success' => '1']);
        }

        if ($cannot) {
            $prod = '';
            foreach ($cannot as $id) {
                $prod .= Product::find($id)->name.',';
            }
            return response()->json(['cannot' => substr($prod,0,-1)]);
        }
        
    }

    public function printBarcode(Request $request)
    {
        $products = array();
        
        foreach($request['id'] as $id){
            $product = Product::find($id);
            $products[] = $product;
        }
        
        $no = 1;
        $pdf = PDF::loadView('products.barcode', compact('products', 'no'));
        $pdf->setPaper('a4', 'potrait');      

        $unique = Str::random(10);

        return $pdf->download('barcode_'.$unique.'.pdf');
    }

    public function activate(Request $request, $id)
    {
        $product = Product::find($id);
        $product->status = $request->status;
        $product->save();

        return response()->json(['success' => '1']);
    }
}
