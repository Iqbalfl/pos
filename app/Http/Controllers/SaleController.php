<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Sale;
use App\SaleDetail;
use App\Inventory;
use DB;
use Session;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $sales = Sale::with('member', 'user');
            return Datatables::of($sales)
                ->addIndexColumn()
                ->addColumn('price_total_f', function ($sale) {
                    return 'Rp '.rupiah_format($sale->price_total);
                })
                ->addColumn('amount_f', function ($sale) {
                    return 'Rp '.rupiah_format($sale->amount);
                })
                ->addColumn('discount_f', function ($sale) {
                    return $sale->discount_member.'%';
                })
                ->addColumn('date_f', function ($sale) {
                    return date('d-m-Y h:i:s', strtotime($sale->created_at));
                })
                ->addColumn('action', function($sale){
                    return view('partials._action', [
                        'model'           => $sale,
                        'form_url'        => route('sale.destroy', $sale->id),
                        'edit_id'         => $sale->id
                    ]);
                })
                ->make(true);
        }

        return view('sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $sale = Sale::findOrFail($id);

        if ($request->ajax()) {
             $no = 0;
            $data = array();
            foreach($sale->details as $detail){
                $no ++;
                $row = array();
                $row[] = $no;
                $row[] = $detail->product->code;
                $row[] = $detail->product->name;
                $row[] = "Rp ".rupiah_format($detail->sell_price);
                $row[] = $detail->qty;
                $row[] = $detail->discount.'%';
                $row[] = "Rp ".rupiah_format($detail->sub_total);
                $data[] = $row;
            }
            
            $output = array("data" => $data);
            return response()->json($output);
        }

       return view('sales.show', ['sale' => $sale]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $sale = Sale::findOrFail($id);

         if ($request->ajax()) {
            $no = 0;
            $data = array();
            foreach($sale->details as $detail){
                $no ++;
                $row = array();
                $row[] = $no;
                $row[] = $detail->product->code;
                $row[] = $detail->product->name;
                $row[] = "Rp ".rupiah_format($detail->sell_price);
                $row[] = "<input type='number' class='form-control' id='qty_$detail->id' value='$detail->qty' onChange='changeCount($detail->id)'>";;
                $row[] = $detail->discount.'%';
                $row[] = "Rp ".rupiah_format($detail->sub_total);
                $row[] = '<a onclick="deleteItem('.$detail->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
            
            $output = array("data" => $data);
            return response()->json($output);
        }

        return view('sales.edit', ['sale' => $sale]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('delete_id')) {
            $sale_detail = SaleDetail::find($request->delete_id);
            
            // kembalikan stok
            $inventory = Inventory::where('product_id', $sale_detail->product_id)->first();
            $inventory->stock = $inventory->stock + $sale_detail->qty;
            $inventory->save();
            
            $sale_detail->delete();
        }

        if ($request->has('update_id')) {
            $sale_detail = SaleDetail::find($request->update_id);

            $inventory = Inventory::where('product_id', $sale_detail->product_id)->first();            
            if ($request->qty > $sale_detail->qty) {
                $qty = $request->qty - $sale_detail->qty;
                /* kurangi stok */
                $inventory->stock = $inventory->stock - $qty;
            } 
            if ($request->qty < $sale_detail->qty) {
                $qty = $sale_detail->qty - $request->qty;
                /* tambah stok */
                $inventory->stock = $inventory->stock + $qty;
            }
            $inventory->save();
            
            $discount = ($sale_detail->discount / 100) * $sale_detail->sell_price;
            $sale_detail->qty = $request->qty;
            $sale_detail->sub_total = ($sale_detail->sell_price * $request->qty) - ($discount * $request->qty);
            $sale_detail->save();
        }

        // update sale
        $sale = Sale::find($id);

        $price_total = 0;
        $item_total = 0;
        foreach ($sale->details as $detail) {
            $price_total += $detail->sub_total;
            $item_total += $detail->qty;
        }

        $sale->item_total = $item_total;
        $sale->price_total = $price_total;
        $discount = ($sale->discount_member / 100) * $price_total;
        $sale->amount = $price_total - $discount;
        $sale->paid = str_replace(".", "", $request->input('paid', $sale->paid));
        $sale->user_id = $request->user()->id;
        $sale->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil update data"
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::find($id);
        
        foreach ($sale->details as $key => $detail) {
            // kembalikan stok
            $inventory = Inventory::where('product_id', $detail->product_id)->first();   
            $inventory->stock = $inventory->stock + $detail->qty;
            $inventory->save();
            
            $detail->delete();
        }
     
        $sale->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('sale.index');
    }

    public function bulkBasePrice()
    {
        try {
                $sale_details = SaleDetail::get();

                $success = 0;
                $fail = 0;
                foreach ($sale_details as $detail) {
                    $product = \App\Product::find($detail->product_id);
                    
                    $detail->base_price = $product->base_price;
                    if ($detail->save()) {
                        ++$success;
                    } else {
                        ++$fail;
                    }
                }

                $data['message'] = 'Query Success : '.$success.', Failed : '.$fail;
                $data['code'] = '200';
                $data['data'] = $sale_details;

                return response()->json($data);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json('Error Model'.$debug=env('APP_DEBUG',false)==true?$e:'');   
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json('Error Query'.$debug=env('APP_DEBUG',false)==true?$e:'');        
            } catch(\ErrorException $e) {
                return response()->json('Error Exception '.$debug=env('APP_DEBUG',false)==true?$e:'');         
            }
    }

    /**
     * Print the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        $transaction = new TransactionController();
        $transaction->printReceipt($id);

       return response()->json(['success' => true]);
    }
}
