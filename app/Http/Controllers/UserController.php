<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\User;
use App\ProductPurchase;
use App\Sale;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::query();
            return Datatables::of($users)
                ->addIndexColumn()
                ->addColumn('avatars', function ($user) {
                    return view('partials._image', [
                        'model'           => $user,
                        'image_url'       => url('img', $user->avatar)
                    ]);
                })
                ->addColumn('action', function($user){
                    return view('partials._action', [
                        'model'           => $user,
                        'form_url'        => route('user.destroy', $user->id),
                        'edit_id'         => $user->id
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required|string',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->avatar = $request->input('avatar', 'user-default.jpg');
            $user->role = $request->role;
            $user->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email,' . $id,
            'password' => 'nullable|min:6|confirmed',
            'role' => 'required|string',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->input('password') != '') {
                $user->password = Hash::make($request->password);
            }
            $user->avatar = $request->input('avatar', 'user-default.jpg');
            $user->role = $request->role;
            $user->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        
        // cek self destroy
        if (\Auth::user()->id == $id) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus diri sendiri :p"
            ]);
            return redirect()->back();
        }

        if ($user->hasRole('warehouse') || $user->hasRole('admin')) {
            // cek pembelian
            $purchase = ProductPurchase::where('user_id',$id)->count();

            if ($purchase > 0) {
                Session::flash("flash_notification", [
                    "level"=>"warning",
                    "message"=>"Tidak dapat menghapus user yang sudah melakukan transaksi pembelian!"
                ]);
                return redirect()->back();
            }
        }

        if ($user->hasRole('cashier')) {
             // cek apakah user sudah melakukan penjualan
            $sale = Sale::where('user_id', $id)->count();
            
            if ($sale > 0) {
                Session::flash("flash_notification", [
                    "level"=>"warning",
                    "message"=>"Tidak dapat menghapus user yang sudah melakukan transaksi penjualan!"
                ]);
                return redirect()->back();
            }
        }
        
        $user->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('user.index');
    }

    public function showProfile()
    {
        $auth = \Auth::user()->id;
        $user = User::find($auth);
        return view('users.profile', ['user' => $user]);
    }

    public function updateProfile(Request $request)
  	{
  		$auth = \Auth::user()->id;
        $user = User::find($auth);
  		
  		$this->validate($request, [
  			'name' => 'required',
  			'email' => 'required|unique:users,email,' . $user->id,
  			'avatar' => 'image|max:1024',
  		]);

        if ($request->hasFile('avatar')) {
            // menambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('avatar');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            // memindahkan file ke folder public/img
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($user->avatar) {
                $old_image = $user->avatar;
                if ($user->avatar != 'user-default.jpg') {
                    # dete if not default
                    $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                    . DIRECTORY_SEPARATOR . $user->avatar;
                    try {
                        File::delete($filepath);
                    } catch (FileNotFoundException $e) {
                        // File sudah dihapus/tidak ada
                    }
                }
            }
            // ganti field image dengan image yang baru
            $user->avatar = $filename;
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');        
        $user->save();
  	
  		Session::flash("flash_notification", [
  			"level"=>"success",
  			"message"=>"Profil berhasil diubah"
  		]);

  		return redirect()->route('profile.show');
    }
      
    public function showPassword()
    {
        return view('users.password');
    }

    public function updatePassword(Request $request){
        $auth = \Auth::user()->id;
        $user = User::find($auth);

        $this->validate($request, [
            'password' => 'required|passcheck:' . $user->password,
            'new_password' => 'required|confirmed|min:6',
        ], [
            'password.passcheck' => 'Password lama tidak sesuai'
        ]);

        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Password berhasil diubah"
        ]);

        return redirect()->route('password.show');
    }
}
