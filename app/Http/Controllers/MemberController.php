<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use PDF;
use App\Member;
use Illuminate\Support\Str;
use Session;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $members = Member::query();
            return Datatables::of($members)
                ->addColumn('checkbox', function ($member) {
                        return '<input type="checkbox" name="id[]" value="'.$member->id.'">';
                    })
                ->addIndexColumn()
                ->addColumn('action', function($member){
                    return view('partials._action', [
                        'model'           => $member,
                        'form_url'        => route('member.destroy', $member->id),
                        'edit_id'         => $member->id
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('members.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'code' => 'required|integer|unique:members',
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $member = new Member;
            $member->code = $request->code;
            $member->name = $request->name;
            $member->address = $request->address;
            $member->phone = $request->phone;
            $member->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);
        return response()->json($member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'code' => 'required|integer|unique:members,code,' . $id,
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            // Store to database  
            $member = Member::find($id);
            $member->code = $request->code;
            $member->name = $request->name;
            $member->address = $request->address;
            $member->phone = $request->phone;
            $member->save();

            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
        $member->delete();

        return redirect()->route('member.index');
    }

    public function printCard(Request $request)
    {
        $members = array();

        foreach($request->id as $id){
            $member = Member::find($id);
            $members[] = $member;
        }
        
        $pdf = PDF::loadView('members.card', compact('members'));
        $pdf->setPaper(array(0, 0, 566.93, 850.39), 'potrait');     

        $unique = Str::random(10);

        return $pdf->download('member-card_'.$unique.'.pdf');
    }
}
