<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\ProductPurchase;
use App\ProductPurchaseDetail;
use App\Inventory;
use App\Supplier;
use App\Product;
use DB;
use Session;

class ProductPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if ($request->ajax()) {
            $purchases = ProductPurchase::with('supplier', 'user');
            return Datatables::of($purchases)
                ->addIndexColumn()
                ->addColumn('price_total_f', function ($purchase) {
                    return 'Rp '.rupiah_format($purchase->price_total);
                })
                ->addColumn('amount_f', function ($purchase) {
                    return 'Rp '.rupiah_format($purchase->amount);
                })
                ->addColumn('discount_f', function ($purchase) {
                    return $purchase->discount.'%';
                })
                ->addColumn('date_f', function ($purchase) {
                    return date('d-m-Y h:i:s', strtotime($purchase->created_at));
                })
                ->addColumn('action', function($purchase){
                    return view('partials._action', [
                        'model'           => $purchase,
                        'form_url'        => route('purchase.destroy', $purchase->id),
                        'edit_id'         => $purchase->id
                    ]);
                })
                ->make(true);
        }

        return view('purchases.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $supplier = Supplier::find($request->supplier_id);

        if (session('carts')) {
            $data = session('carts');
        } else {
            $data = null;
        }

        if ($request->ajax()) {
            return view('purchases.cart', ['data' => $data])->render();
        }

        return view('purchases.transaction')->with(compact('data', 'supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // lock db
        DB::beginTransaction();

        // create purchase
        $purchase = new ProductPurchase;

        $purchase->invoice = ProductPurchase::generateInvoice();
        
        if ($request->supplier_id == '' ) {
            DB::rollback();
            Session::flash("flash_notification", [
                "level"=>"error",
                "message"=>"Gagal Supplier Tidak Boleh Kosong!"
            ]);
            return redirect()->back();
        }

        $purchase->supplier_id = $request->supplier_id;
        $purchase->item_total = $request->item_total;
        $purchase->price_total = str_replace(".", "", $request->price_total);
        $purchase->discount = $request->discount;
        $purchase->amount = str_replace(".", "", $request->amount);
        $purchase->user_id = $request->user()->id;
        
        if ($purchase->save()) {
             // create purchase details
            if (session('carts')) {
                $cart = session()->get('carts');
                
                foreach ($cart as $key => $item) {

                    $purchase_detail = new ProductPurchaseDetail;
                    $purchase_detail->product_purchase_id = $purchase->id;
                    $purchase_detail->product_id = $item['id'];
                    $purchase_detail->base_price = $item['base_price'];
                    $purchase_detail->qty = $item['quantity'];
                    $purchase_detail->sub_total = $item['base_price']*$item['quantity'];
                    $purchase_detail->save();

                    // increase stock
                    $inventory = Inventory::where('product_id', $item['id'])->first();
                    $inventory->stock = $inventory->stock+$item['quantity'];
                    $inventory->save();
                }

            } else {
                DB::rollback();
                Session::flash("flash_notification", [
                    "level"=>"error",
                    "message"=>"Gagal Keranjang Kosong!"
                ]);
                return redirect()->back();
            }
        } else {
            DB::rollback();
            Session::flash("flash_notification", [
                "level"=>"error",
                "message"=>"Gagal Terjadi Kesalahan Saat Menyimpan data!"
            ]);
            return redirect()->back();
        }
        
        DB::commit();
        // clear session
        session()->forget('carts');

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan pembelian"
        ]);

        return redirect()->route('purchase.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $purchase = ProductPurchase::findOrFail($id);

        if ($request->ajax()) {
             $no = 0;
            $data = array();
            foreach($purchase->details as $detail){
                $no ++;
                $row = array();
                $row[] = $no;
                $row[] = $detail->product->code ?? 'produk dihapus';
                $row[] = $detail->product->name ?? 'produk dihapus';
                $row[] = "Rp ".rupiah_format($detail->base_price);
                $row[] = $detail->qty;
                $row[] = "Rp ".rupiah_format($detail->sub_total);
                $data[] = $row;
            }
            
            $output = array("data" => $data);
            return response()->json($output);
        }

       return view('purchases.show', ['purchase' => $purchase]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $purchase = ProductPurchase::findOrFail($id);

        if ($request->ajax()) {
            $no = 0;
            $data = array();
            foreach($purchase->details as $detail){
                $no ++;
                $row = array();
                $row[] = $no;
                $row[] = $detail->product->code ?? 'produk dihapus';
                $row[] = $detail->product->name ?? 'produk dihapus';
                $row[] = "Rp ".rupiah_format($detail->base_price);
                $row[] = "<input type='number' class='form-control' id='qty_$detail->id' value='$detail->qty' onChange='changeCount($detail->id)'>";;
                $row[] = "Rp ".rupiah_format($detail->sub_total);
                $row[] = '<a onclick="deleteItem('.$detail->id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
            
            $output = array("data" => $data);
            return response()->json($output);
        }

        return view('purchases.edit', ['purchase' => $purchase]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('delete_id')) {
            $purchase_detail = ProductPurchaseDetail::find($request->delete_id);
            
            // kembalikan stok
            $inventory = Inventory::where('product_id', $purchase_detail->product_id)->first();
            $inventory->stock = $inventory->stock - $purchase_detail->qty;
            $inventory->save();
            
            $purchase_detail->delete();
        }

        if ($request->has('update_id')) {
            $purchase_detail = ProductPurchaseDetail::find($request->update_id);

            $inventory = Inventory::where('product_id', $purchase_detail->product_id)->first();            
            if ($request->qty > $purchase_detail->qty) {
                $qty = $request->qty - $purchase_detail->qty;
                /* tambah stok */
                $inventory->stock = $inventory->stock + $qty;
            } 
            if ($request->qty < $purchase_detail->qty) {
                $qty = $purchase_detail->qty - $request->qty;
                /* kurangi stok */
                $inventory->stock = $inventory->stock - $qty;
            }
            $inventory->save();
            
            $purchase_detail->qty = $request->qty;
            $purchase_detail->sub_total = $purchase_detail->base_price * $request->qty;
            $purchase_detail->save();
        }

        // update purchase
        $purchase = ProductPurchase::find($id);

        $price_total = 0;
        $item_total = 0;
        foreach ($purchase->details as $detail) {
            $price_total += $detail->sub_total;
            $item_total += $detail->qty;
        }

        $purchase->item_total = $item_total;
        $purchase->price_total = $price_total;
        $discount = ($purchase->discount / 100) * $price_total;
        $purchase->amount = $price_total - $discount;
        $purchase->user_id = $request->user()->id;
        $purchase->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil update data"
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchase = ProductPurchase::find($id);
        
        foreach ($purchase->details as $key => $detail) {
            // kembalikan stok
            $inventory = Inventory::where('product_id', $detail->product_id)->first();
            if ($inventory != null){
                $inventory->stock = $inventory->stock - $detail->qty;
                $inventory->save();
            }
            
            $detail->delete();
        }
     
        $purchase->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('purchase.index');
    }

    public function showSupplier(Request $request)
    {
        $suppliers = \App\Supplier::query();
        return Datatables::of($suppliers)
            ->addIndexColumn()
            ->addColumn('action', function ($supplier) {
                return '<a href="purchase/create?supplier_id='.$supplier->id.'" class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a>';
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function showProduct(Request $request)
    {
        $products = Product::where('status', 100);
        return Datatables::of($products)
            ->addColumn('action', function ($product) {
                return '<a onclick=selectItem("'.$product->code.'") class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a>';
            })
            ->addColumn('base_price_rupiah', function ($product) {
                return ('Rp '.rupiah_format($product->base_price));
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function addToCart($code)
    {
        $product = Product::where('code',$code)->firstOrFail();
        
        $cart = session()->get('carts');
        
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                    $code => [
                        "id" => $product->id,
                        "name" => $product->name,
                        "code" => $product->code,
                        "quantity" => 1,
                        "base_price" => $product->base_price
                    ]
            ];

            session()->put('carts', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$code])) {
 
            $cart[$code]['quantity']++;
 
            session()->put('carts', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$code] = [
            "id" => $product->id,
            "name" => $product->name,
            "code" => $product->code,
            "quantity" => 1,
            "base_price" => $product->base_price
        ];
 
        session()->put('carts', $cart);
 
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function updateCart(Request $request, $code)
    {
        if($request->code and $request->quantity)
        {
            $cart = session()->get('carts');
 
            $cart[$request->code]["quantity"] = $request->quantity;
 
            session()->put('carts', $cart);
        }
    }

    public function removeItemCart(Request $request, $code)
    {
        if($request->code) {
 
            $cart = session()->get('carts');
 
            if(isset($cart[$request->code])) {
 
                unset($cart[$request->code]);
 
                session()->put('carts', $cart);
            }

        }
    }

    public function loadFormat(Request $request)
    {
        $amount_terbilang = ucwords(terbilang($request->amount)).' Rupiah';
        return response()->json(['amount_terbilang' => $amount_terbilang]);
    }
}
