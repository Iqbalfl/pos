<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Sale;
use App\SaleDetail;
use App\Inventory;
use App\Product;
use App\Member;
use App\Setting;
use Session;
use DB;
use Mike42\Escpos\Printer; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use App\Prints\Item;

class TransactionController extends Controller
{
    public function index(Request $request)
    {   
        $setting = Setting::first();

        if (session('cart')) {
            $data = session('cart');
        } else {
            $data = null;
        }

        if ($request->ajax()) {
            return view('transactions.cart', ['data' => $data])->render();
        }

        return view('transactions.index')->with(compact('data', 'setting'));
    }

    public function store(Request $request)
    {
        // lock db
        DB::beginTransaction();

        // create sale
        $sale = new Sale;

        // check member id
        if ($request->member_code != 0 && $request->member_code != '') {
            $member = Member::where('code',$request->member_code)->first();
            if ($member) {
                $member_id = $member->id;
            } else {
                $member_id = null;
            }
        } else {
            $member_id = null;
        }

        $sale->invoice = Sale::generateInvoice();
        $sale->member_id = $member_id;
        $sale->item_total = $request->item_total;
        $sale->price_total = str_replace(".", "", $request->price_total);
        $sale->discount_member = $request->discount_member;
        $sale->amount = str_replace(".", "", $request->amount);
        $sale->paid = str_replace(".", "", $request->paid);
        $sale->user_id = $request->user()->id;
        
        if ($sale->save()) {
             // create sale details
            if (session('cart')) {
                $cart = session()->get('cart');
                
                foreach ($cart as $key => $item) {
                    $discount_nominal = ($item['discount']/100)*$item['sell_price']*$item['quantity'];

                    $sale_detail = new SaleDetail;
                    $sale_detail->sale_id = $sale->id;
                    $sale_detail->product_id = $item['id'];
                    $sale_detail->base_price = Product::find($item['id'])->base_price;
                    $sale_detail->sell_price = $item['sell_price'];
                    $sale_detail->qty = $item['quantity'];
                    $sale_detail->discount = $item['discount'];
                    $sale_detail->sub_total = $item['sell_price']*$item['quantity']-$discount_nominal;
                    $sale_detail->save();

                    // decrease stock
                    $inventory = Inventory::where('product_id', $item['id'])->first();
                    $inventory->stock = $inventory->stock-$item['quantity'];
                    $inventory->save();
                }

            } else {
                DB::rollback();
                Session::flash("flash_notification", [
                    "level"=>"error",
                    "message"=>"Gagal Keranjang Kosong!"
                ]);
                return redirect()->back();
            }
        } else {
            DB::rollback();
            Session::flash("flash_notification", [
                "level"=>"error",
                "message"=>"Gagal Terjadi Kesalahan Saat Menyimpan data!"
            ]);
            return redirect()->back();
        }
        
        DB::commit();
        // clear session
        session()->forget('cart');

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan penjualan"
        ]);

        if (Setting::first()->receipt == 1){
            $this->printReceipt($sale->id);
        }

        return redirect()->route('transaction.index');
    }

    public function showProduct(Request $request)
    {
        $products = Product::where('status', 100);
        return Datatables::of($products)
            ->addColumn('action', function ($product) {
                return '<a onclick=selectItem("'.$product->code.'") class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a>';
            })
            ->addColumn('sell_price_rupiah', function ($product) {
                return ('Rp '.rupiah_format($product->sell_price));
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function showMember(Request $request)
    {
        $members = Member::query();
        return Datatables::of($members)
            ->addColumn('action', function ($member) {
                return '<a onclick="selectMember('.$member->code.')" class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a>';
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function addToCart($code)
    {
        $product = Product::where('code',$code)->firstOrFail();
        
        $cart = session()->get('cart');
        
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                    $code => [
                        "id" => $product->id,
                        "name" => $product->name,
                        "code" => $product->code,
                        "quantity" => 1,
                        "sell_price" => $product->sell_price,
                        "discount" => $product->discount
                    ]
            ];

            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$code])) {
 
            $cart[$code]['quantity']++;
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$code] = [
            "id" => $product->id,
            "name" => $product->name,
            "code" => $product->code,
            "quantity" => 1,
            "sell_price" => $product->sell_price,
            "discount" => $product->discount
        ];
 
        session()->put('cart', $cart);
 
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function updateCart(Request $request, $code)
    {
        if($request->code and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->code]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
        }
    }

    public function removeItemCart(Request $request, $code)
    {
        if($request->code) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->code])) {
 
                unset($cart[$request->code]);
 
                session()->put('cart', $cart);
            }

        }
    }

    public function loadFormat(Request $request)
    {
        $terbilang = ucwords(terbilang($request->to_terbilang ?? 0)).' Rupiah';
        $rupiah_format = rupiah_format($request->to_rupiah ?? 0);
        return response()->json(['rupiah_format' => $rupiah_format, 'terbilang' => $terbilang]);
    }

    public function printReceipt($sale_id)
    {
        try {
            /* Get Data Setting */
            $setting = Setting::first();

            /* Get Data to print */
            $sale = Sale::find($sale_id);

            if (!$sale){
                 Session::flash("flash_notification", [
                    "level"=>"error",
                    "message"=>"Print Gagal Data Tidak Ada!"
                ]);
                return redirect()->back();
            }

            // Enter the share name for your USB printer here
            $printer_name = $setting->printer_name ?? 'POS-58'; // Nama Printer yang di sharing
            $connector = new WindowsPrintConnector($printer_name);
            
            /* Start the printer */
            $printer = new Printer($connector);

            /* Information for the receipt */
            $item_prices = [];
            foreach ($sale->details as $detail) {
                if ($detail->discount != 0) {
                    $discount = ' - Disc '.$detail->discount.'%';
                } else {
                    $discount = '';
                }
                $item_prices[] = new item(rupiah_format($detail->sell_price).' x '.$detail->qty.$discount, rupiah_format($detail->sub_total), $detail->product->name);
            }

            $subtotal = new item('Subtotal', rupiah_format($sale->price_total));
            $discount_member = new item('Diskon Member', '-'.rupiah_format(($sale->discount_member/100)*$sale->price_total));
            $total = new item('Total', rupiah_format($sale->amount));
            $paid = new item('Dibayar', rupiah_format($sale->paid));
            $cashback = new item('Kembali', rupiah_format($sale->paid - $sale->amount));
            /* Date is kept the same for testing */
            $date = now()->format('d/m/Y h:i:s A');

            /* Name of shop */
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer -> text($setting->store_name."\n");
            $printer -> selectPrintMode();
            $printer -> text($setting->store_address."\n");
            if ($setting->store_phone != '' || $setting->store_phone != null) {
                $printer -> text($setting->store_phone."\n");
            }
            $printer -> feed();
           
            /* Title of receipt */
            $printer -> text($sale->invoice."\n");
            $printer -> text($date."\n");
            $printer -> text("Kasir : ".\Auth::user()->name."\n");
            $printer -> feed();
           
            /* Items */
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> setEmphasis(true);
            $printer -> text(new item('Items', 'Rp'));
            $printer -> setEmphasis(false);
            $printer -> text("--------------------------------\n");
            
            foreach ($item_prices as $item) {
                $printer -> text($item."\n");
            }

            $printer -> text("================================\n");
            $printer -> setEmphasis(true);
            $printer -> text($subtotal);
            $printer -> setEmphasis(false);
           
            /* Tax and total */
            $printer -> text($discount_member);
            $printer -> setEmphasis(true);
            $printer -> text($total);
            $printer -> setEmphasis(false);
            $printer -> text($paid);
            $printer -> text($cashback);
            $printer -> selectPrintMode();
           
            /* Footer */
            $printer -> feed(2);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> text($setting->store_message."\n");
           
            /* Cut the receipt and open the cash drawer */
            $printer -> feed(4);
            if ($setting->cashdrawer == 1) {
                $printer -> pulse();
            }
            $printer -> close();
        } catch (Exception $e) {
            Session::flash("flash_notification", [
                "level"=>"error",
                "message"=>"Print Gagal : " . $e->getMessage() . "\n"
            ]);
            return redirect()->back();
        }
    }
}