<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Category;
use App\Product;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $categories = Category::query();
            return Datatables::of($categories)
                ->addIndexColumn()
                ->addColumn('action', function($category){
                    return view('partials._action', [
                        'model'           => $category,
                        'form_url'        => route('category.destroy', $category->id),
                        'edit_id'        => $category->id
                    ]);
                })
                ->make(true);
        }

        return view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->passes()) {
            // Store to database  
            $category = new Category;
            $category->name = $request->name;
            $category->save();
            
            return response()->json(['success' => '1']);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response()->json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->passes()) {
            // Store to database  
            $category = Category::findOrFail($id);
            $category->name = $request->name;
            $category->save();

            return response()->json(['success' => true]);
        }
        
        return response()->json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        
        // cek apakah kategori di pakai oleh produk
        $product = Product::where('category_id', $id)->count();
        
        if ($product > 0) {
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak dapat menghapus kategori yang sudah dikaitkan ke produk!"
            ]);
            return redirect()->back();
        }

        $category->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('category.index');
    }
}
