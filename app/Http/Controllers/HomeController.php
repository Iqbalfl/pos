<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductPurchase;
use App\Product;
use App\Supplier;
use App\Sale;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $from = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
        $to = date('Y-m-d');

        $date = $from;
        $date_data = array();
        $income_data = array();

        while(strtotime($date) <= strtotime($to)){ 
            $date_data[] = (int)substr($date,8,2);
            
            $income = Sale::where('created_at', 'LIKE', "$date%")->sum('amount');
            $income_data[] = (int) $income;

            $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
        }
        
        $count['purchase'] = ProductPurchase::count();
        $count['product'] = Product::count();
        $count['supplier'] = Supplier::count();
        $count['sales'] = Sale::count();

        $products = Product::select(\DB::raw('products.id, products.code, products.name, categories.name as category, SUM(sale_details.qty) as sales'))
                    ->join('categories', 'categories.id', '=', 'products.category_id')
                    ->join('sale_details', 'sale_details.product_id', '=', 'products.id')
                    ->groupBy('products.id')
                    ->orderBy('sales', 'desc')
                    ->limit(10)->get();

        if(Auth::user()->hasRole('admin')) 
            return view('dashboard.admin', compact('count', 'from', 'to', 'date_data', 'income_data', 'products'));
        else if(Auth::user()->hasRole('cashier')) 
            return view('dashboard.cashier');
        else
            return view('dashboard.warehouse');
    }
}
