<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductPurchase;
use App\Sale;
use App\Setting;
use App\Expenditure;
use PDF;

class ReportController extends Controller
{
    public function indexIncome(Request $request)
    {
        if ($request->has('start_at') && $request->has('end_at')) {
            $start_at = $request->start_at;
            $end_at = $request->end_at;
        } else {
            $start_at = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
            $end_at = date('Y-m-d');
        }
        
        $no = 0;
        $data = array();
        $income = 0;
        $loss = 0;
        $tmp = 0;
        $start = $start_at;
        while(strtotime($start) <= strtotime($end_at)){ 
            $date = $start;
            $start = date('Y-m-d', strtotime("+1 day", strtotime($start)));

            $total_sales = Sale::where('created_at', 'LIKE', "$date%")->sum('amount');
            $total_purchase = ProductPurchase::where('created_at', 'LIKE', "$date%")->sum('amount');
            $total_expenditure = Expenditure::where('created_at', 'LIKE', "$date%")->sum('nominal');

            $tmp = $total_sales - $total_purchase - $total_expenditure;
            if ($tmp < 0){
                $loss = $tmp;
                $income = 0;
            } else {
                $income = $tmp;
                $loss = 0; 
            }

            $no ++;
            $row = array();
            $row['no'] = $no;
            $row['date'] = date('d-m-Y', strtotime($date));
            $row['total_sale'] = $total_sales;
            $row['total_purchase'] = $total_purchase;
            $row['total_expenditure'] = $total_expenditure;
            $row['total_income'] = $income;
            $row['total_loss'] = $loss;
            $data[] = $row;
        }
        return view('reports.income')->with(compact('data', 'start_at', 'end_at'));
    }

    public function pdfIncome(Request $request)
    {
        if ($request->has('start_at') && $request->has('end_at')) {
            $start_at = $request->start_at;
            $end_at = $request->end_at;
        } else {
            $start_at = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
            $end_at = date('Y-m-d');
        }
        
        $no = 0;
        $data = array();
        $income = 0;
        $loss = 0;
        $tmp = 0;
        $start = $start_at;
        while(strtotime($start) <= strtotime($end_at)){ 
            $date = $start;
            $start = date('Y-m-d', strtotime("+1 day", strtotime($start)));

            $total_sales = Sale::where('created_at', 'LIKE', "$date%")->sum('amount');
            $total_purchase = ProductPurchase::where('created_at', 'LIKE', "$date%")->sum('amount');
            $total_expenditure = Expenditure::where('created_at', 'LIKE', "$date%")->sum('nominal');

            $tmp = $total_sales - $total_purchase - $total_expenditure;
            if ($tmp < 0){
                $loss = $tmp;
                $income = 0;
            } else {
                $income = $tmp;
                $loss = 0; 
            }

            $no ++;
            $row = array();
            $row['no'] = $no;
            $row['date'] = date('d-m-Y', strtotime($date));
            $row['total_sale'] = $total_sales;
            $row['total_purchase'] = $total_purchase;
            $row['total_expenditure'] = $total_expenditure;
            $row['total_income'] = $income;
            $row['total_loss'] = $loss;
            $data[] = $row;
        }

        $setting = Setting::first();

        $pdf = PDF::loadView('reports.pdf_income', compact('start_at', 'end_at', 'data', 'setting'));
        $pdf->setPaper('a4', 'potrait');
        
        return $pdf->stream();
    }

    public function indexSales(Request $request)
    {
        if ($request->has('start_at') && $request->has('end_at')) {
            $start_at = $request->start_at;
            $end_at = $request->end_at;
        } else {
            $start_at = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
            $end_at = date('Y-m-d');
        }
        
        $sales = Sale::with('details', 'user')
                    ->whereDate('created_at', '>=', $start_at)
                    ->whereDate('created_at', '<=', $end_at)
                    ->get();

        $data = [];
        if ($sales->isNotEmpty()) {
            foreach ($sales as $sale) {
                $row = [];
                $base_price_total = 0;
                $row['date'] = $sale->created_at->format('d-m-Y');
                $row['invoice'] = $sale->invoice;
                $row['discount_member'] = ($sale->discount_member / 100) * $sale->price_total;
                $row['total'] = $sale->amount;
                foreach ($sale->details as $detail) {
                    $base_price_total += $detail->base_price*$detail->qty;
                }
                $row['laba'] = $sale->details->sum('sub_total') - $base_price_total - $row['discount_member'];
                $row['user'] = $sale->user->name;
                $data[] = $row;
            }
        }
        
        return view('reports.sales')->with(compact('data', 'start_at', 'end_at'));
    }

    public function pdfSales(Request $request)
    {
        if ($request->has('start_at') && $request->has('end_at')) {
            $start_at = $request->start_at;
            $end_at = $request->end_at;
        } else {
            $start_at = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
            $end_at = date('Y-m-d');
        }
        
        $sales = Sale::with('details', 'user')
                    ->whereDate('created_at', '>=', $start_at)
                    ->whereDate('created_at', '<=', $end_at)
                    ->get();

        $data = [];
        if ($sales->isNotEmpty()) {
            foreach ($sales as $sale) {
                $row = [];
                $base_price_total = 0;
                $row['date'] = $sale->created_at->format('d-m-Y');
                $row['invoice'] = $sale->invoice;
                $row['discount_member'] = ($sale->discount_member / 100) * $sale->price_total;
                $row['total'] = $sale->amount;
                foreach ($sale->details as $detail) {
                    $base_price_total += $detail->base_price*$detail->qty;
                }
                $row['laba'] = $sale->details->sum('sub_total') - $base_price_total - $row['discount_member'];
                $row['user'] = $sale->user->name;
                $data[] = $row;
            }
        }
        
        $setting = Setting::first();

        $pdf = PDF::loadView('reports.pdf_sales', compact('start_at', 'end_at', 'data', 'setting'));
        $pdf->setPaper('a4', 'potrait');
        
        return $pdf->stream();
    }

}
