<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPurchaseDetail extends Model
{
    use SoftDeletes;

    public function purchase(){
        return $this->belongsTo('App\ProductPurchase');
    }

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
