<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $appends = ['role_human'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($name)
    {
        if ($this->role === $name){
            return true;
        } 
         
        return false;
    }

    public function getRoleHumanAttribute()
    {
        if ($this->attributes['role'] != null) {
            if ($this->attributes['role'] == 'admin') {
                $role = 'Administrator';
            } elseif ($this->attributes['role'] == 'warehouse') {
                $role = 'Gudang';
            } elseif ($this->attributes['role'] == 'cashier') {
                $role = 'Kasir';
            }
        } else {
            return $this->attributes['role'];
        }

        return $role;
    }
}
