<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function member(){
        return $this->belongsTo('App\Member');
    }

    public function details(){
        return $this->hasMany('App\SaleDetail');
    }

    public function scopeGenerateInvoice()
    {
        $unique = false;
        $prefix = 'INV';

        while ($unique == false) {
            $code = base_convert(microtime(), 10, 36);
            $randomID = $prefix.'-'.date('ym').'-'. strtoupper(substr(uniqid($code), 0, 6));

            $check = $this->where('invoice',$randomID)->count();
            if ($check > 0) {
                $unique = false;
            } else {
                $unique = true;
            }
        }
        return $randomID;
    }
}
