<?php

namespace App\Prints;
use Illuminate\Support\Str;

class Item {
    private $name;
    private $price;
    private $add;
    public function __construct($add = '', $price = '', $name = '')
    {
        $this -> add = $add;
        $this -> price = $price;
        $this -> name = $name;
    }
    
    public function __toString()
    {
        $rightCols = 10; // adjust
        $leftCols = 22; // adjust
        
        $name = Str::limit($this -> name, 29);
        $left = str_pad($this -> add, $leftCols) ;
        $right = str_pad($this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$name\n$left$right";
    }
}