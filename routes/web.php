<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'main', 'middleware'=>['auth']], function () {	
	/* member */
	Route::post('/member/print', 'MemberController@printCard');
	
	/* setting */
	Route::get('/setting', 'SettingController@form')->name('setting.form');
	Route::post('/setting', 'SettingController@save')->name('setting.save');
	
	/* purchase */
	Route::get('/purchase/show-supplier', 'ProductPurchaseController@showSupplier')->name('purchase.show-supplier');
	Route::get('/purchase/show-product', 'ProductPurchaseController@showProduct')->name('purchase.show-product');
	Route::post('/purchase/product/{code}', 'ProductPurchaseController@addToCart')->name('purchase.add-cart');
	Route::patch('/purchase/product/{code}', 'ProductPurchaseController@updateCart')->name('purchase.update-cart');
	Route::delete('/purchase/product/{code}', 'ProductPurchaseController@removeItemCart')->name('purchase.remove-item-cart');
	Route::post('/purchase/load-format', 'ProductPurchaseController@loadFormat')->name('purchase.load-format');

	/* sale */
	Route::get('sale', 'SaleController@index')->name('sale.index');
	Route::get('sale/bulk', 'SaleController@bulkBasePrice');
	Route::get('sale/{id}', 'SaleController@show')->name('sale.show');
	Route::get('sale/{id}/print', 'SaleController@print')->name('sale.print');
	Route::get('sale/{id}/edit', 'SaleController@edit')->name('sale.edit')->middleware('role:admin');
	Route::patch('sale/{id}', 'SaleController@update')->name('sale.update')->middleware('role:admin');
	Route::delete('sale/{id}', 'SaleController@destroy')->name('sale.destroy')->middleware('role:admin');

	/* product */
	Route::post('/product/delete-selected', 'ProductController@deleteSelected')->middleware('role:admin');
   	Route::post('/product/print-barcode', 'ProductController@printBarcode');
   	Route::post('/product/{id}/activate', 'ProductController@activate');
	Route::get('product', 'ProductController@index')->name('product.index');
	Route::post('product', 'ProductController@store')->name('product.store');
	Route::get('product/{id}', 'ProductController@show')->name('product.show');
	Route::get('product/{id}/edit', 'ProductController@edit')->name('product.edit')->middleware('role:admin');
	Route::patch('product/{id}', 'ProductController@update')->name('product.update')->middleware('role:admin');
	Route::delete('product/{id}', 'ProductController@destroy')->name('product.destroy')->middleware('role:admin');
	
	/* profile */
	Route::get('profile', 'UserController@showProfile')->name('profile.show');
	Route::post('profile', 'UserController@updateProfile')->name('profile.update');
	Route::get('profile/password', 'UserController@showPassword')->name('password.show');
	Route::post('profile/password', 'UserController@updatePassword')->name('password.update');

	Route::resource('category', 'CategoryController')->middleware('role:admin');
	Route::resource('member', 'MemberController')->middleware('role:admin');
	Route::resource('supplier', 'SupplierController')->middleware('role:admin');
	Route::resource('expenditure', 'ExpenditureController')->middleware('role:admin');
	Route::resource('user', 'UserController')->middleware('role:admin');

	Route::resource('purchase', 'ProductPurchaseController');
	
	// laporan
	Route::get('/report/income', 'ReportController@indexIncome')->name('report.income.index');
	Route::get('/report/income/pdf', 'ReportController@pdfIncome')->name('report.income.pdf');
	Route::get('/report/sales', 'ReportController@indexSales')->name('report.sales.index');
	Route::get('/report/sales/pdf', 'ReportController@pdfSales')->name('report.sales.pdf');

	Route::group(['middleware'=>['role:cashier']], function () {
		Route::get('/transaction', 'TransactionController@index')->name('transaction.index');
		Route::post('/transaction', 'TransactionController@store')->name('transaction.store');
		Route::get('/transaction/print/{sale_id}', 'TransactionController@printReceipt')->name('transaction.print');
		Route::get('/transaction/show-product', 'TransactionController@showProduct')->name('transaction.show-product');
		Route::get('/transaction/show-member', 'TransactionController@showMember')->name('transaction.show-member');
		Route::post('/transaction/product/{code}', 'TransactionController@addToCart')->name('transaction.add-cart');
		Route::patch('/transaction/product/{code}', 'TransactionController@updateCart')->name('transaction.update-cart');
		Route::delete('/transaction/product/{code}', 'TransactionController@removeItemCart')->name('transaction.remove-item-cart');
		Route::post('/transaction/load-format', 'TransactionController@loadFormat')->name('transaction.load-format');
	});
});